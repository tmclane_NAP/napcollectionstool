﻿Imports System
Imports System.Data
Imports System.Data.Odbc
Imports System.Collections
Imports System.Web
Imports System.IO
Imports NAPCollectionsTool.cSystem

#Disable Warning IDE1006 ' Naming Styles
Public Class cInit
#Enable Warning IDE1006 ' Naming Styles


    'Programmer - TMcLane
    'Date       - 02/2018
    'Description  - created class. Copied from NAPRatingGroupTool

    Public Shared AppIni As New cIniFile

    Public Shared sODBC As String

    Public Shared iInitVendorMax As Integer
    Public Shared iSendDaysPlus As Integer
    'HE-24 start
    Public Shared iSecondHRSendDays As Integer
    Public Shared iSecondHRSendDayofWeek As Integer
    'HE-24 end


    Public Shared nRatingGroupMarginDays As Short

    Public Shared sIPDataBase As String

    Public Shared arCOMUSER_SECURITY_CODES() As String = {"CI", "ER"}
    Public Const APP_NAME = "NAPCollectionsTool"
    Public Const APP_INI_FILE_NAME = "NAPCollectionsTool.ini"
    Public Const COMUSER_PARTITION_GROUP = "COM USER"
    Public Const COMUSER_ID = "napcom"


#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function init() As Boolean
#Enable Warning IDE1006 ' Naming Styles

        If setAppIniFile() = False Then
            Return False
        End If

        If setABPIniFile() = False Then
            Return False
        End If

        If setVariables() = False Then
            Return False
        End If

        If setSqlConnection() = False Then
            Return False
        End If

        If validateComUser() = False Then
            Return False
        End If

        If setRevenueManagerConnection() = False Then
            Return False
        End If



        Return True

    End Function

#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function setAppIniFile() As Boolean
#Enable Warning IDE1006 ' Naming Styles

        Try
            AppIni.Load(Directory.GetCurrentDirectory() & "\" & APP_INI_FILE_NAME)
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
            Return False
        End Try

        Return True

    End Function

#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function setVariables() As Boolean
#Enable Warning IDE1006 ' Naming Styles

        Try
            'Validating configuration file
            If AppIni.Sections.Count <> 2 Then
                writeToEventLog("Invalid " & APP_INI_FILE_NAME & ".", EventLogEntryType.Error)
                Return False
            End If

            If AppIni.GetSection("Database").Keys.Count <> 1 Then
                writeToEventLog("Invalid " & APP_INI_FILE_NAME & ".", EventLogEntryType.Error)
                Return False
            End If

            'If AppIni.GetSection("Main").Keys.Count <> 1 Then
            'writeToEventLog("Invalid " & APP_INI_FILE_NAME & ".", EventLogEntryType.Error)
            'Return False
            'End If

            'Get variables from configuration file
            If AppIni.GetSection("Database").GetKey("ODBC").Value = "" Then
                writeToEventLog("ODBC entry missing in " & APP_INI_FILE_NAME & ".  Please edit the ini file and set the 'ODBC' key to the correct User DSN.", EventLogEntryType.Warning)
                Return False
            Else
                sODBC = AppIni.GetSection("Database").GetKey("ODBC").Value
            End If


            If AppIni.GetSection("Main").GetKey("VENDORMAX").Value = "" Then
                iInitVendorMax = 12
                writeToEventLog("Value missing for VENDORMAX in " & APP_INI_FILE_NAME & " using default of 12", EventLogEntryType.Warning)
            Else
                iInitVendorMax = CInt(AppIni.GetSection("Main").GetKey("VENDORMAX").Value)
            End If



        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
            Return False
        End Try

        Return True

    End Function
End Class
