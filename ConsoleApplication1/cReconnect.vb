﻿Imports System.Data.Odbc
Imports NAPCollectionsTool.cInit
Imports NAPCollectionsTool.cFunctions
Imports NAPCollectionsTool.cSystem
Imports RMAPI = Nexant.BillingAPI
Imports CoreAPI = Nexant.CoreAPI
#Disable Warning IDE1006 ' Naming Styles
Public Class cReconnect
#Enable Warning IDE1006 ' Naming Styles

    'Programmer - TMcLane
    'Date       - 01/2018
    'Description  - created class

    Public Shared Sub Reconnect()
        'Change Log
        '
        'ProRrammer    - TMcLane
        'Date          - 10/2015
        'Creation:   Created Reconnect()
        'RM Object Variables
        Dim oRVendMgr As RMAPI.VendorMgr = Nothing
        Dim oRVendor As RMAPI.Vendor = Nothing
        Dim oRVendors As RMAPI.VendorList = Nothing
        Dim oRCustomers As RMAPI.CustomerList
        Dim oRCustomer As RMAPI.Customer
        Dim oRAccount As RMAPI.Account
        Dim oRLDC As RMAPI.LdcAccount
        Dim oRCustCrit As RMAPI.CustomerCriteria = Nothing
        Dim oRTranactions As RMAPI.TransactionList = Nothing
        Dim oRTransaction As RMAPI.Transaction = Nothing
        Dim oRTransCrit As RMAPI.TransactionCriteria = Nothing
        Dim oREBTCRIT As RMAPI.EbtTransactionCriteria = Nothing
        Dim oREBTs As RMAPI.EdiTransactionList = Nothing
        Dim oREBT As RMAPI.EdiTransaction = Nothing
        Dim oRESPBILLDET As RMAPI.EspBillDeterminant = Nothing
        'Dim oRMeterList As RMAPI.MeterList = Nothing
        'Dim oRMeter As RMAPI.Meter = Nothing
        Dim oRContactlist As CoreAPI.ContactList = Nothing
        Dim oRContact As CoreAPI.Contact = Nothing


        'ODBC variables
        Dim GetComm As OdbcCommand
        Dim GetRS As OdbcDataReader


        'Boolean Variables
        Dim bUDFUpdate As Boolean = False
        Dim bFoundPay As Boolean = False
        Dim bByPassLDC As Boolean = False

        'String Variables

        Dim sRMessage As String = " "
        Dim sRSubType As String = " "
        Dim sVendorName As String = " "
        Dim sReconDaysZero As String = CStr(iInitVendorMax)
        Dim sToday As String = Nothing
        Dim sTodayMinus As String = Nothing
        Dim sGetDropQuery As String = Nothing
        Dim sInteraction As String = Nothing
        Dim sDNPAmount As String = Nothing
        Dim sMeter As String = Nothing
        Dim aMeter() As String = {}
        'Numeric Variables
        Dim iRUpdatedCount As Integer = 0
        Dim iRCountFromQuery As Integer = 0
        Dim iRLDCBypassedCount As Integer = 0
        Dim iRLDCBypassedDuetoUDF As Integer = 0
        Dim iRVendorBypassedCount As Integer = 0
        Dim iRREconnectAlreadyCreated As Integer = 0
        Dim iRPaymentsFound As Integer = 0
        Dim iRPaymentsNotFound As Integer = 0
        Dim iRPaymentNotEnough As Integer = 0
        Dim iRInteractionsAddedCount As Integer = 0
        Dim iREInteractionsAddedCount As Integer = 0
        Dim iRWInteractionsAddedCount As Integer = 0
        Dim iRECONDays As Integer = -12
        Dim iUDFCount As Integer = 0
        Dim iTodayMinus As Integer = 0
        Dim iToday As Integer = 0
        Dim iVendorEBTs As Integer = 0
        Dim iLength As Integer = 0
        Dim iCount As Integer = 0
        Dim iOffSet As Integer = 0
        Dim iPositionDollar As Integer = 0
        Dim iPositionDecimal As Integer = 0
        Dim dUnbilled As Decimal = 0
        Dim dCur As Decimal = 0
        Dim d0 As Decimal = 0
        Dim d30 As Decimal = 0
        Dim d60 As Decimal = 0
        Dim d90 As Decimal = 0
        Dim dTot As Decimal = 0
        Dim dUnapplied As Decimal = 0
        Dim dBal As Decimal = 0
        Dim dDNPAmount As Decimal = 0
        Dim dLDCBalance As Decimal = 0
        Dim dAbsoluteValue As Decimal = 0
        Dim dTotalPay As Decimal = 0
        Dim uOriginalDisconnect As ULong = 0
        Dim uContactId As ULong = 0
        Dim imeteroffset As Integer = 0




        writeToBatchLog(0, "Collections Tool  Reconnect: Beginning Process at  " & Now())

        Try



            Try  'Obtaining data to check
                sToday = Today.ToString("yyyyMMdd")
                sTodayMinus = Today.AddDays(iRECONDays).ToString("yyyyMMdd")

                iToday = CInt(sToday)
                iTodayMinus = CInt(sTodayMinus)


                sGetDropQuery = " select v.vendor_nm,  u.VALUE_TX,  i.INACT_SUB_TY_CD, i.INACT_TY_CD, i.INACT_TX as INACT_TX, i.INTACT_DT,  l.STATUS_CD, l.LDC_ACCT_ID as LDC_ACCT_ID , l.acct_id as ACCT_ID , l.LDC_ACCT_NO, e.EDI_TRANS_ID As OriginalTransID,  " &
                                " e.RESPONSE_STATUS_CD, e.status_cd as EDI_STATUS,e.TRANS_TY,  e.TRANS_SUB_TY,e.CREATION_DT as EDI_CREATION_DATE, e.LDC_ACCT_NO, e.LDC_ACCT_ID as EDI_LDC_ACCT_ID, e.LDC_VENDOR_ID  as EDI_LDC_VENOR_ID, ud.Value_TX as BypassReconFlag " &
                                " from EDI_TRANSACTION e " &
                                " join USER_FIELD_DATA u on u.related_id = e.ldc_vendor_id and u.field_def_id = 4101 and u.VALUE_TX  <> '0' " &
                                " join LDC_ACCOUNT l on l.LDC_ACCT_ID = e.LDC_ACCT_ID and l.STATUS_CD = 'ACT' " &
                                " join INTERACTION i on i.RELATE_ID = l.LDC_ACCT_ID and i.CLASS_NM = 'cLDCAccount' and i.INACT_TX like '%DNP%Late Amount%$%Next Event%MOVE OUT on%'  and i.INACT_TY_CD = 'CCEX' " &
                                " join VENDOR v on v.VENDOR_ID = e.LDC_VENDOR_ID" &
                                " left join USER_FIELD_DATA ud on ud.RELATED_ID = e.LDC_ACCT_ID and ud.VALUE_TX = 'Y' and ud.FIELD_def_ID = 4037  and ud.START_DT <= " & iToday & " and (ud.END_DT = 0 or ud.END_DT > " & iToday & " ) " &
                                " where e.CREATION_DT >= " & iTodayMinus & " and i.INTACT_DT >= " & iTodayMinus & " and e.TRANS_TY = 'OR'  and e.TRANS_SUB_TY = '72' " &
                                " and RESPONSE_STATUS_CD = 'WQ' and e.STATUS_CD = 'CLOS' " &
                                " order by e.LDC_VENDOR_ID, e.CREATION_DT, e.LDC_ACCT_ID"

                GetComm = New OdbcCommand(sGetDropQuery, dbconn)
                GetRS = GetComm.ExecuteReader()
                With GetRS
                    If .HasRows Then
                        Do While .Read
                            iRCountFromQuery = iRCountFromQuery + 1
                            If Not IsDBNull(.Item("BypassReconFlag")) Then
                                If .Item("BypassReconFlag") = "Y" Then
                                    iRLDCBypassedDuetoUDF = iRLDCBypassedDuetoUDF + 1
                                    Continue Do
                                End If
                            End If
                            oRCustCrit = RMAPI.Customer.GetCustomerCriteria()
                            oRCustCrit.LdcAccountId = .Item("EDI_LDC_ACCT_ID")
                            oRCustCrit.LdcAccountVendorId = .Item("EDI_LDC_VENOR_ID")
                            oRCustomers = RMAPI.Customer.GetCustomers(oRCustCrit)
                            If oRCustomers.Count = 0 Then
                                writeToBatchLog(0, "Collections Tool  Reconnect: No LDC accounts found for processing for LDC ACCT ID  = " & .Item("LDC_ACCT_ID"))
                                Continue Do
                            End If
                            For Each oRCustomer In oRCustomers
                                uContactId = Nothing
                                For Each oRAccount In oRCustomer.GetAccountList
                                    oRContactlist = oRAccount.GetContacts()
                                    For Each oRContact In oRContactlist
                                        uContactId = oRContact.ObjectId
                                        Exit For
                                    Next
                                    'Call oRAccount.GetBalanceInfo(dUnbilled, dCur, d0, d30, d60, d90, dTot, dUnapplied, dBal)
                                    For Each oRLDC In oRAccount.GetLDCAccounts
                                        If oRLDC.ObjectId = .Item("EDI_LDC_ACCT_ID") Then
                                            iPositionDecimal = 0
                                            iPositionDollar = 0
                                            iOffSet = 0
                                            iLength = 0
                                            dDNPAmount = 0
                                            sInteraction = Nothing
                                            sDNPAmount = Nothing
                                            uOriginalDisconnect = Nothing
                                            bByPassLDC = False
                                            bFoundPay = False
                                            sInteraction = Nothing
                                            sInteraction = .Item("INACT_TX")
                                            iLength = Len(sInteraction)
                                            '$ always starts in 30 but let's leave it more flexible
                                            iPositionDollar = InStr(sInteraction, "$")
                                            iPositionDecimal = InStr(sInteraction, ".")
                                            'the length of the amount is the decimal place starting column  plus 3 (for the decial and the cents) minus the starting position of the number which is plus 1 to where the $ sign is found
                                            'if the $ is in 30 the number starts in 31. If the decimal is in 33 the cents end after 36.  36 - 31 = a lenght of 5 for a value like 56.99
                                            iLength = (iPositionDecimal + 3) - (iPositionDollar + 1)
                                            ' offsets in substring are one less than the position so use the $ position as the offset to the start of the amount
                                            iOffSet = iPositionDollar
                                            sDNPAmount = sInteraction.Substring(iOffSet, iLength)
                                            If (sDNPAmount = Nothing) Or (Not IsNumeric(sDNPAmount)) Then
                                                sRMessage = "Collections Tool  Reconnect: Could determine amount of Drop due to Non Payment  =  "
                                                sRSubType = "CTRE"
                                                If CreateInteraction(oRCustomer, oRLDC, sRMessage, sRSubType) = True Then
                                                    iREInteractionsAddedCount = iREInteractionsAddedCount + 1
                                                End If
                                                iRLDCBypassedCount = iRLDCBypassedCount + 1
                                                bByPassLDC = True
                                                Exit For
                                            Else
                                                dDNPAmount = dDNPAmount + CDec(sDNPAmount)
                                            End If
                                            oRTransCrit = RMAPI.Transaction.GetTransactionCriteria()
                                            oRTransCrit.AcctId = .Item("ACCT_ID")
                                            oRTransCrit.LdcAcctId = .Item("EDI_LDC_ACCT_ID")
                                            oRTransCrit.FromTransDate = convToDateTime(.Item("EDI_CREATION_DATE"))
                                            oRTransCrit.ToTransDate = convToDateTime(iToday)
                                            oRTranactions = oRLDC.GetTransactionsByCriteria(oRTransCrit)
                                            For Each oRTransaction In oRTranactions
                                                If oRTransaction.TransactionTypeCode = "PAY" Then
                                                    dTotalPay = oRTransaction.TransactionAmount
                                                    bFoundPay = True
                                                    Continue For  ' maybe the customer made more than 1 payment. 
                                                Else
                                                    Continue For ' try next transaction this one was not a PAY
                                                End If
                                            Next 'tranasaction
                                            If bByPassLDC = True Then
                                                Continue For   'we had an error on ths LDC go on to the next one
                                            ElseIf bFoundPay = True Then ' we found at least one PAY TRANS. See if we can reconnect 
                                                iRPaymentsFound = iRPaymentsFound + 1
                                                dLDCBalance = oRLDC.Balance
                                                If dTotalPay < 0 Then
                                                    dAbsoluteValue = (dTotalPay * -1)
                                                Else
                                                    'this should never happen but what the heck!
                                                    dAbsoluteValue = dTotalPay
                                                End If
                                                If (dAbsoluteValue >= dDNPAmount) Or dLDCBalance < 30.0 Then
                                                    oREBTCRIT = oRLDC.GetEBTTransactionCriteria
                                                    oREBTCRIT.RequestFromDate = convToDateTime(.Item("EDI_CREATION_DATE"))
                                                    oREBTCRIT.RequestToDate = convToDateTime(iToday)
                                                    oREBTCRIT.SearchLimit = 1
                                                    oREBTCRIT.TransType = "OR"
                                                    oREBTCRIT.TransSubType = "79"
                                                    oREBTCRIT.LdcAcctId = oRLDC.ObjectId
                                                    oREBTs = oRLDC.GetEDITransactions(oREBTCRIT)
                                                    If oREBTs.Count > 0 Then
                                                        'we created a RECONNECT already either manually or automated.
                                                        iRREconnectAlreadyCreated = iRREconnectAlreadyCreated + 1
                                                    Else
                                                        'TMClane 7/13/2018 -- Commented out all code on Meters
                                                        '                     TX only requires LDC_ACCT_ID / ESI ID.
                                                        '                      set aMeter to nothing
                                                        'oRMeterList = oRLDC.GetMeters
                                                        'writeToBatchLog(0, "Collections Tool  MeterList Count = " & oRMeterList.Count)
                                                        'ReDim aMeter(oRMeterList.Count - 1)
                                                        'For Each oRMeter In oRMeterList
                                                        '    imeteroffset = 0
                                                        '    If oRMeterList.Count = 1 Then
                                                        '        aMeter(imeteroffset) = CStr(oRMeter.MeterNo)
                                                        '        Exit For
                                                        '    Else
                                                        '        If oRMeter.IsActive = True Then
                                                        '            aMeter(imeteroffset) = CStr(oRMeter.MeterNo)
                                                        '            imeteroffset = imeteroffset + 1
                                                        '            'writeToBatchLog(0, "Collections Tool  offset =  " & imeteroffset)
                                                        '        End If
                                                        '    End If

                                                        'Next

                                                        'If aMeter Is Nothing Then
                                                        '    sRMessage = "Collections Tool  Reconnect: Could determine Meter "
                                                        '    sRSubType = "CTRE"
                                                        '    If CreateInteraction(oRCustomer, oRLDC, sRMessage, sRSubType) = True Then
                                                        '        iREInteractionsAddedCount = iREInteractionsAddedCount + 1
                                                        '    End If
                                                        '    iRLDCBypassedCount = iRLDCBypassedCount + 1
                                                        '    bByPassLDC = True
                                                        '    Exit For 'get out of LDC after writing error interaction
                                                        'End If
                                                        oRESPBILLDET = oRLDC.GetCurrentESPBillDeterminant()
                                                        uOriginalDisconnect = .Item("OriginalTransID")
                                                        'writeToBatchLog(0, "Collections Tool Got to just before 650 create")
                                                        oRLDC.CreateEBTServiceOrderRequest(Today, oRESPBILLDET.VendorDBNo, "79", "IT", "RC01", Nothing, Nothing, "", uOriginalDisconnect, uContactId, aMeter, Today)
                                                        sRMessage = "Collections Tool  Reconnect: Service Order EBT Reconnect created due to recent payment   =  " & CStr(dAbsoluteValue)
                                                        sRSubType = "CTRC"
                                                        If CreateInteraction(oRCustomer, oRLDC, sRMessage, sRSubType) = True Then
                                                            iRInteractionsAddedCount = iRInteractionsAddedCount + 1
                                                        End If
                                                    End If

                                                Else
                                                    iRPaymentNotEnough = iRPaymentNotEnough + 1
                                                    Continue For  ' NExt LDC
                                                End If
                                            ElseIf bFoundPay = False Then
                                                iRPaymentsNotFound = iRPaymentsNotFound + 1
                                            End If
                                        Else
                                            Continue For 'try next LDC ACCCT ID
                                        End If

                                    Next  ' LDC
                                Next 'ACCT
                            Next ' Customer


                        Loop  'Next SQL read
                    Else
                        writeToBatchLog(0, "Collections Tool  Reconnect: No Service Order Disconnects between " & sTodayMinus & " " & sToday & " at " & Now())
                    End If
                End With 'with SQL execute
TerminateGet:
                Try
                    GetRS.Close()
                    GetComm.Dispose()
                Catch ex As Exception
                    writeToBatchLog(0, "Collections Tool  Reconnect: Error = " & ex.Message)
                End Try ' dispose of  ODBC

                writeToBatchLog(0, "Collections Tool  Reconnect: Number of Service Order Disconnects between " & sTodayMinus & " " & sToday & " = " & CStr(iRCountFromQuery))

                iVendorEBTs = 0
            Catch exO As Exception  'Try Obtain Data
                writeToBatchLog(2, "Collections Tool  Reconnect: Getting Data " & exO.Message)
                writeToEventLog(exO.Message, EventLogEntryType.Error)
            End Try ' Obtain Data

            writeToBatchLog(0, "Collections Tool  Reconnect:  Number of Completed Reconnect Interactions  Written   = " & iRInteractionsAddedCount)
            writeToBatchLog(0, "Collections Tool  Reconnect:  Number of Error Interactions  Written  = " & iREInteractionsAddedCount)
            writeToBatchLog(0, "Collections Tool  Reconnect:  Number of Warning Interactions  Written   = " & iRWInteractionsAddedCount)
            writeToBatchLog(0, "Collections Tool  Reconnect:  Number of LDC Accounts with Payments Found =  " & iRPaymentsFound)
            writeToBatchLog(0, "Collections Tool  Reconnect:  Number of LDC Accounts with Payments NOT Found =  " & iRPaymentsNotFound)
            writeToBatchLog(0, "Collections Tool  Reconnect:  Number of LDC Accounts Bypassed due Reconnect Bypass Flag   = " & iRLDCBypassedDuetoUDF)
            writeToBatchLog(0, "Collections Tool  Reconnect:  Number of LDC Accounts Bypassed due to Errors   = " & iRLDCBypassedCount)
            writeToBatchLog(0, "Collections Tool  Reconnect:  Number of LDC Accounts Bypassed due to Payment Not Enough and Bal GT $30   = " & iRPaymentNotEnough)
            writeToBatchLog(0, "Collections Tool  Reconnect:  Number of LDC Accounts Bypassed due to Reconnect Already Created   = " & iRREconnectAlreadyCreated)
            writeToBatchLog(0, "Collections Tool  Reconnect:  Finished processing at " & Now())
        Catch ex As Exception  ' sub try
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try ' sub try
        writeToBatchLog(0, "Collections Tool  Reconnect: Ending Sub  " & Now())

    End Sub
End Class
