﻿Imports System.Data
Imports System.Data.Odbc
Imports NAPCollectionsTool.cInit
Imports NAPCollectionsTool.cSystem
Imports RMAPI = Nexant.BillingAPI
Imports CoreAPI = Nexant.CoreAPI


#Disable Warning IDE1006 ' Naming Styles
Public Class cFunctions
#Enable Warning IDE1006 ' Naming Styles

    'Change Log
    '
    'Date: 02/2018
    'Programmer: T. McLane
    'Description:   Created cFunctions by copying cFunctions from NAPRatingGroupTool
    'Reason:  RM Enhancements



    'RM Objects

    Public Shared Function validateReadCycle(ByVal sVendorId As String, ByVal sReadCycle As String, ByVal dLdcStartDt As Date) As Date

        'This function will return a date or nothing.  When a date is returned, the ldc read cycle is valid. 
        Dim dResult As Date = Nothing
        Dim sQry As String
        Dim comm As OdbcCommand
        Dim rs As OdbcDataReader

        sQry = "select cycle_dt" &
                " from cycle" &
                " where vendor_id = '" & sVendorId & "'" &
                " and cycle_ty_cd = 'READ'" &
                " and cycle_no = '" & sReadCycle & "'"

        comm = New OdbcCommand(sQry, dbconn)
        rs = comm.ExecuteReader()

        With rs
            If .HasRows Then
                Do While .Read
                    If convToDateTime(.Item("cycle_dt")) >= dLdcStartDt.AddDays(-5) And convToDateTime(.Item("cycle_dt")) <= dLdcStartDt.AddDays(5) Then
                        'Found matching Read Cycle read date for LDC account start date - store it and terminate.
                        dResult = convToDateTime(.Item("cycle_dt"))
                        GoTo Terminate
                        'TMcLane_0414
                    Else
                        'Use the date outside the  + / - 5 days to differenitate from noRows returned and keep looking for a better match
                        dResult = convToDateTime(.Item("cycle_dt"))
                        'TMcLane_0414 end
                    End If
                Loop
            Else
                'LDC account does not have a valid Read Cycle-  return 'Nothing' in dresult
                GoTo Terminate
            End If
        End With


Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        validateReadCycle = dResult

    End Function

#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function getEndDateFromReadCycle(ByVal sVendorId As String, ByVal sReadCycle As String, ByVal dLdcStartDt As Date, ByVal nPromoRateMonths As Integer) As Date
#Enable Warning IDE1006 ' Naming Styles

        Dim dResult As Date = Nothing
        Dim sQry As String
        Dim comm As OdbcCommand
        Dim rs As OdbcDataReader
        Dim dPromoEndDate As Date = Nothing

        dPromoEndDate = dLdcStartDt.AddMonths(nPromoRateMonths)

        sQry = "select cycle_dt" &
                " from cycle" &
                " where vendor_id = '" & sVendorId & "'" &
                " and cycle_ty_cd = 'READ'" &
                " and cycle_no = '" & sReadCycle & "'"

        comm = New OdbcCommand(sQry, dbconn)
        rs = comm.ExecuteReader()
        rs.Read()

        With rs
            If .HasRows Then
                Do While .Read
                    If convToDateTime(.Item("cycle_dt")) >= dPromoEndDate.AddDays(-5) And convToDateTime(.Item("cycle_dt")) <= dPromoEndDate.AddDays(5) Then
                        'Found matching Read Cycle read date for LDC account end date.
                        'commented out subtracting 1 day from last read cycle and added code to use the read cycle date
                        'dResult = convToDateTime(.Item("cycle_dt")).AddDays(-1)
                        dResult = convToDateTime(.Item("cycle_dt"))
                        GoTo Terminate
                    End If
                Loop
            Else
                'LDC account does not have a valid Read Cycle.
                GoTo Terminate
            End If
        End With

Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        getEndDateFromReadCycle = dResult

    End Function

#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function getEndDateFromReadCycleFend(ByVal sVendorId As String, ByVal sReadCycle As String, sPlanType As String, ByVal dLdcStartDt As Date, ByVal dRGEndDate As Date) As Date
#Enable Warning IDE1006 ' Naming Styles

        Dim dResult As Date = Nothing
        Dim sQry As String
        Dim comm As OdbcCommand
        Dim rs As OdbcDataReader
        Dim dEndDate As Date = Nothing
        Dim iDay As Integer
        Dim iPrevDay As Integer

        dEndDate = dRGEndDate
        iDay = dEndDate.Day
        'get the previous day of the month and make it a negative.
        iPrevDay = (iDay - 1) * -1
        'get the first day of the month. It is always the current day plus a negative of the previous day such as 31 + -30 = 1  o 27  + -26 = 1, and so on
        dEndDate = dEndDate.AddDays(iPrevDay)


        sQry = "select cycle_dt" &
                " from cycle" &
                " where vendor_id = '" & sVendorId & "'" &
                " and cycle_ty_cd = 'READ'" &
                " and cycle_no = '" & sReadCycle & "'"

        comm = New OdbcCommand(sQry, dbconn)
        rs = comm.ExecuteReader()
        rs.Read()

        With rs
            If .HasRows Then
                Do While .Read
                    'Get the first read cycle of the month
                    If convToDateTime(.Item("cycle_dt")) >= dEndDate Then
                        dResult = convToDateTime(.Item("cycle_dt"))
                        GoTo Terminate
                    End If
                Loop
            Else
                'LDC account does not have a valid Read Cycle.
                GoTo Terminate
            End If
        End With

Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        getEndDateFromReadCycleFend = dResult

    End Function
#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function getFutureReadcycle(ByVal sVendorId As String, ByVal sReadCycle As String, ByVal dFutureDt As Date) As Date
#Enable Warning IDE1006 ' Naming Styles

        'This function will return a date or nothing.  When a date is returned, the ldc read cycle is valid. 
        Dim dResult As Date = Nothing
        Dim sQry As String
        Dim comm As OdbcCommand
        Dim rs As OdbcDataReader
        Dim sToday As String
        Dim iToday As Integer

        sToday = Today.ToString("yyyyMMdd")
        iToday = CInt(sToday)

        sQry = "select cycle_dt" &
                " from cycle" &
                " where vendor_id = '" & sVendorId & "'" &
                " and cycle_ty_cd = 'READ'" &
                " and cycle_dt >= " & iToday &
                " and cycle_no = '" & sReadCycle & "'"

        comm = New OdbcCommand(sQry, dbconn)
        rs = comm.ExecuteReader()

        With rs
            If .HasRows Then
                Do While .Read
                    'The first cycle date GT or EQ to the future date is the one we want
                    If convToDateTime(.Item("cycle_dt")) >= dFutureDt Then
                        'Found Read Cycle read date - store it and terminate.
                        dResult = convToDateTime(.Item("cycle_dt"))
                        GoTo Terminate
                    Else
                        dResult = Nothing
                    End If
                Loop
            Else
                'LDC account does not have a valid Read Cycle-  return 'Nothing' in dresult
                dResult = Nothing
                GoTo Terminate
            End If
        End With


Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        getFutureReadcycle = dResult

    End Function

    Public Shared Function GetPlanType(ByVal sPlanCd As String) As String


        'This function will return a string  or nothing.  When a string  is returned we found the planType in IP
        Dim dResult As String = Nothing
        Dim sQry As String = Nothing
        Dim comm As OdbcCommand = Nothing
        Dim rs As OdbcDataReader = Nothing
        Dim sEndDate As String = Nothing
        Dim dEndDate As Date = Nothing


        Try
            sQry = "select lower(rtrim(ip2.planType)) as planType, ip2.end_date from OPENQUERY(" &
                    sIPDataBase & ", 'select ip.planType, cast(ip.planenddate as char) as end_date " &
                   " from napower.providers_rates_index ip where ip.planCode  = """ & sPlanCd & """ ') ip2"



            comm = New OdbcCommand(sQry, dbconn)
            rs = comm.ExecuteReader()

            With rs
                If .HasRows Then
                    Do While .Read
                        If Not IsDBNull(.Item("planType")) Then
                            'if the plan end date is null then use it, else find the null one.....loop
                            'this is needed because sometimes we reuse non fend planCodes.
                            If .Item("end_date") = "0000-00-00" Or IsDBNull(.Item("end_date")) Then
                                dResult = .Item("planType")
                                GoTo Terminate
                            Else
                                sEndDate = .Item("end_date")
                                'remove first "-"
                                sEndDate = sEndDate.Remove(4, 1)
                                'remove second "-"  - it goes from postion 7 in original to 6 after removal of first "-"
                                sEndDate = sEndDate.Remove(6, 1)
                                dEndDate = convToDateTime(sEndDate)
                                'writeToBatchLog(0, "Rating Group Tool (cFunctions):  string date =  " & sEndDate & " converted date = " & dEndDate)
                                If Today < dEndDate Then
                                    dResult = .Item("planType")
                                    GoTo Terminate
                                Else
                                    dResult = Nothing
                                End If

                            End If
                        End If
                    Loop
                Else
                    dResult = Nothing
                    GoTo Terminate
                End If
            End With

        Catch ex As Exception
            writeToBatchLog(1, "Rating Group Tool (cFunctions): Could not get PlanType From " & sIPDataBase & " napower.providers_rates_index due to " & ex.Message)
            dResult = Nothing
        End Try
Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        GetPlanType = dResult


    End Function
    'new function GetIsRenewal Flag for rate code
    Public Shared Function GetIsRenewal(ByVal sPlanCd As String) As String


        'This function will return a string  or nothing.  When a string  is returned we found the planType in IP
        Dim dResult As String = Nothing
        Dim sQry As String = Nothing
        Dim comm As OdbcCommand = Nothing
        Dim rs As OdbcDataReader = Nothing
        Dim sEndDate As String = Nothing
        Dim dEndDate As Date = Nothing


        Try
            sQry = "select lower(rtrim(ip2.isRenewal)) as isRenewal, ip2.end_date from OPENQUERY(" &
                    sIPDataBase & ", 'select ip.isRenewal, cast(ip.planenddate as char) as end_date " &
                   " from napower.providers_rates_index ip where ip.planCode  = """ & sPlanCd & """ ') ip2"



            comm = New OdbcCommand(sQry, dbconn)
            rs = comm.ExecuteReader()

            With rs
                If .HasRows Then
                    Do While .Read
                        If Not IsDBNull(.Item("isRenewal")) Then
                            'if the plan end date is null then use it, else find the null one.....loop
                            'this is needed because sometimes we reuse non fend planCodes.
                            If .Item("end_date") = "0000-00-00" Or IsDBNull(.Item("end_date")) Then
                                dResult = .Item("isRenewal")
                                GoTo Terminate
                            Else
                                sEndDate = .Item("end_date")
                                'remove first "-"
                                sEndDate = sEndDate.Remove(4, 1)
                                'remove second "-"  - it goes from postion 7 in original to 6 after removal of first "-"
                                sEndDate = sEndDate.Remove(6, 1)
                                dEndDate = convToDateTime(sEndDate)
                                'writeToBatchLog(0, "Rating Group Tool (cFunctions):  string date =  " & sEndDate & " converted date = " & dEndDate)
                                If Today < dEndDate Then
                                    dResult = .Item("isRenewal")
                                    GoTo Terminate
                                Else
                                    dResult = Nothing
                                End If

                            End If
                        End If
                    Loop
                Else
                    dResult = Nothing
                    GoTo Terminate
                End If
            End With

        Catch ex As Exception
            writeToBatchLog(1, "Rating Group Tool (cFunctions): Could not get IsRenewal From " & sIPDataBase & " napower.providers_rates_index due to " & ex.Message)
            dResult = Nothing
        End Try
Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        GetIsRenewal = dResult


    End Function ' IsRenewal
    'New Function - GetDescription
    Public Shared Function GetDescription(ByVal sPlanCd As String) As String


        'This function will return a string  or nothing.  When a string  is returned we found the planType in IP
        Dim dResult As String = Nothing
        Dim sQry As String = Nothing
        Dim comm As OdbcCommand = Nothing
        Dim rs As OdbcDataReader = Nothing
        Dim sEndDate As String = Nothing
        Dim dEndDate As Date = Nothing
        Dim sLikeStr As String = "%non-renewal%"

        Try
            sQry = "select ip2.description as description, ip2.end_date from OPENQUERY(" &
                    sIPDataBase & ", 'select ip.description, cast(ip.planenddate as char) as end_date " &
                   " from napower.providers_rates_index ip where ip.planCode  = """ & sPlanCd & """  and ip.description like """ & sLikeStr & """ ') ip2"



            comm = New OdbcCommand(sQry, dbconn)
            rs = comm.ExecuteReader()

            With rs
                If .HasRows Then
                    Do While .Read
                        If Not IsDBNull(.Item("description")) Then
                            'if the plan end date is null then use it, else find the null one.....loop
                            'this is needed because sometimes we reuse non fend planCodes.
                            If .Item("end_date") = "0000-00-00" Or IsDBNull(.Item("end_date")) Then
                                dResult = .Item("description")
                                GoTo Terminate
                            Else
                                sEndDate = .Item("end_date")
                                'remove first "-"
                                sEndDate = sEndDate.Remove(4, 1)
                                'remove second "-"  - it goes from postion 7 in original to 6 after removal of first "-"
                                sEndDate = sEndDate.Remove(6, 1)
                                dEndDate = convToDateTime(sEndDate)
                                'writeToBatchLog(0, "Rating Group Tool (cFunctions):  string date =  " & sEndDate & " converted date = " & dEndDate)
                                If Today < dEndDate Then
                                    dResult = .Item("description")
                                    GoTo Terminate
                                Else
                                    dResult = Nothing
                                End If

                            End If
                        End If
                    Loop
                Else
                    dResult = Nothing
                    GoTo Terminate
                End If
            End With

        Catch ex As Exception
            writeToBatchLog(1, "Rating Group Tool (cFunctions): Could not get IsRenewal From " & sIPDataBase & " napower.providers_rates_index due to " & ex.Message)
            dResult = Nothing
        End Try
Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        GetDescription = dResult


    End Function ' GetDescription
    'Start IT-7579
    'New Function - GetDescription_By_ID
    Public Shared Function GetDescription_By_Id(ByVal iRateIndexId As Integer) As String


        'This function will return a string  or nothing.  When a string  is returned we found the planType in IP
        Dim dResult As String = Nothing
        Dim sQry As String = Nothing
        Dim comm As OdbcCommand = Nothing
        Dim rs As OdbcDataReader = Nothing
        Dim sEndDate As String = Nothing
        Dim dEndDate As Date = Nothing
        Dim sLikeStr As String = "%non-renewal%"

        Try
            sQry = "select ip2.description as description, ip2.end_date from OPENQUERY(" &
                    sIPDataBase & ", 'select ip.description, cast(ip.planenddate as char) as end_date " &
                   " from napower.providers_rates_index ip where ip.id = " & iRateIndexId & " and ip.description like """ & sLikeStr & """ ') ip2"



            comm = New OdbcCommand(sQry, dbconn)
            rs = comm.ExecuteReader()

            With rs
                If .HasRows Then
                    Do While .Read
                        If Not IsDBNull(.Item("description")) Then
                            'if the plan end date is null then use it, else find the null one.....loop
                            'this is needed because sometimes we reuse non fend planCodes.
                            If .Item("end_date") = "0000-00-00" Or IsDBNull(.Item("end_date")) Then
                                dResult = .Item("description")
                                GoTo Terminate
                            Else
                                sEndDate = .Item("end_date")
                                'remove first "-"
                                sEndDate = sEndDate.Remove(4, 1)
                                'remove second "-"  - it goes from postion 7 in original to 6 after removal of first "-"
                                sEndDate = sEndDate.Remove(6, 1)
                                dEndDate = convToDateTime(sEndDate)
                                'writeToBatchLog(0, "Rating Group Tool (cFunctions):  string date =  " & sEndDate & " converted date = " & dEndDate)
                                If Today < dEndDate Then
                                    dResult = .Item("description")
                                    GoTo Terminate
                                Else
                                    dResult = Nothing
                                End If

                            End If
                        End If
                    Loop
                Else
                    dResult = Nothing
                    GoTo Terminate
                End If
            End With

        Catch ex As Exception
            writeToBatchLog(1, "Rating Group Tool (cFunctions): Could not get Description From " & sIPDataBase & " napower.providers_rates_index due to " & ex.Message)
            dResult = Nothing
        End Try
Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        GetDescription_By_Id = dResult

    End Function ' GetDescription_By_ID  - End IT-7579


    'new function - get external rate code from price plan
    'start TM20170619
    Public Shared Function GetExternalRateCd(ByVal sPlanCd As String) As String


        'This function will return a string  or nothing.  When a string  is returned we found the planType in IP
        Dim dResult As String = Nothing
        Dim sQry As String = Nothing
        Dim comm As OdbcCommand = Nothing
        Dim rs As OdbcDataReader = Nothing
        Dim dEndDate As Date = Nothing


        Try
            sQry = "select START_DT, END_DT, PRICE_PLAN_CD, EXTERNAL_RATE_CD" &
                 " from PRICE_PLAN " &
                 " where PRICE_PLAN_CD = '" & sPlanCd & "'"




            comm = New OdbcCommand(sQry, dbconn)
            rs = comm.ExecuteReader()



            With rs

                If .HasRows Then

                    Do While .Read

                        'if the price plan end date is 0 or in the future then use it, else find the open one via looping
                        'this is needed because sometimes we reuse planCodes.
                        If .Item("END_DT") > 0 Then
                            dEndDate = convToDateTime(.Item("END_DT"))
                            'Price plan is ended  before today return nothing and get out
                            'else treat a future dated end date the the same as no end date
                            If Today > dEndDate Then
                                dResult = Nothing
                                Continue Do 'this must be a resused rate  -- get next rate
                            End If
                        End If
                        ' End date is 0  or greater than today process
                        'if External_Rate_Cd is null set result to nothing and get out
                        If IsDBNull(.Item("EXTERNAL_RATE_CD")) Then
                            dResult = Nothing
                            Exit Do
                        Else ' if it is not null is it not spaces?
                            If .Item("EXTERNAL_RATE_CD") > " " Then
                                dResult = .Item("EXTERNAL_RATE_CD")
                                'batch log is just for testing
                                'writeToBatchLog(0, "Rating Group Tool (cFunctions): Got External Rate Code from Price Plan.  Price Plan = " & sPlanCd & " and  External Rate Code = " & .Item("EXTERNAL_RATE_CD"))
                                Exit Do   ' get out we are done
                            Else
                                dResult = Nothing   'External Rate Code is spaces - set it to Nothing
                                Exit Do  ' Get out we are done
                            End If
                        End If

                    Loop
                Else
                    dResult = Nothing
                    GoTo Terminate
                End If
            End With

        Catch ex As Exception
            writeToBatchLog(1, "Rating Group Tool (cFunctions): Could not get External RateCode from Price Plan Table  for " & sPlanCd & " " & ex.Message)
            dResult = Nothing
        End Try
Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        GetExternalRateCd = dResult


    End Function 'end TM20170619

    Public Shared Function GetCurrentRate(ByVal sPlanCd As String) As String


        'This function will return a string  or nothing.  When a string  is returned we found the planType in IP
        Dim dResult As String = Nothing
        Dim sQry As String = Nothing
        Dim comm As OdbcCommand = Nothing
        Dim rs As OdbcDataReader = Nothing
        Dim sEndDate As String = Nothing
        Dim dEndDate As Date = Nothing

        Try
            sQry = " select ltrim(rtrim(convert(varchar(20),ip2.currentRate))) As CurrentRate, ip2.end_date from OPENQUERY(" &
                    sIPDataBase & ", 'select ip.currentRate, cast(ip.planenddate as char) as end_date " &
                   " from napower.providers_rates_index ip where ltrim(rtrim(ip.planCode)) = """ & sPlanCd & """ ') ip2"


            comm = New OdbcCommand(sQry, dbconn)
            rs = comm.ExecuteReader()

            With rs
                If .HasRows Then
                    Do While .Read
                        If Not IsDBNull(.Item("CurrentRate")) Then
                            'if the plan end date is null then use it, else find the null one.....loop
                            'this is needed because sometimes we reuse non fend  planCodes.
                            If .Item("end_date") = "0000-00-00" Or IsDBNull(.Item("end_date")) Then
                                dResult = .Item("CurrentRate")
                                GoTo Terminate
                            Else
                                'if .Item("end_date" is not null, is it in the future?  If yes use it, else get next row
                                sEndDate = .Item("end_date")
                                'remove first "-"
                                sEndDate = sEndDate.Remove(4, 1)
                                'remove second "-"  - it goes from postion 7 in original to 6 after removal of first "-"
                                sEndDate = sEndDate.Remove(6, 1)
                                dEndDate = convToDateTime(sEndDate)
                                If Today < dEndDate Then
                                    dResult = .Item("CurrentRate")
                                    GoTo Terminate
                                Else
                                    dResult = Nothing
                                End If
                            End If
                        End If
                    Loop
                Else
                    dResult = Nothing
                    GoTo Terminate
                End If
            End With

        Catch ex As Exception
            writeToBatchLog(1, "Rating Group Tool (cFunctions): Could not get CurrentRate From " & sIPDataBase & " napower.providers_rates_index due to " & ex.Message)
            dResult = Nothing
        End Try
Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        GetCurrentRate = dResult


    End Function
    ' get current rate via rate index id
    Public Shared Function GetCurrentRate_by_id(ByVal iRateIndexId As Integer) As String


        'This function will return a string  or nothing.  When a string  is returned we found the planType in IP
        Dim dResult As String = Nothing
        Dim sQry As String = Nothing
        Dim comm As OdbcCommand = Nothing
        Dim rs As OdbcDataReader = Nothing
        Dim sEndDate As String = Nothing
        Dim dEndDate As Date = Nothing

        Try
            sQry = " select ltrim(rtrim(convert(varchar(20),ip2.currentRate))) As CurrentRate,  ip2.end_date  from OPENQUERY(" &
                    sIPDataBase & ", 'select ip.currentRate, cast(ip.planenddate as char) as end_date " &
                   " from napower.providers_rates_index ip where  ip.id = " & iRateIndexId & " ') ip2"


            comm = New OdbcCommand(sQry, dbconn)
            rs = comm.ExecuteReader()

            With rs
                If .HasRows Then
                    Do While .Read
                        If Not IsDBNull(.Item("CurrentRate")) Then
                            'if the plan end date is null then use it, else find the null one.....loop
                            'this is needed because sometimes we reuse non fend  planCodes.
                            If .Item("end_date") = "0000-00-00" Or IsDBNull(.Item("end_date")) Then
                                dResult = .Item("CurrentRate")
                                GoTo Terminate
                            Else
                                'if .Item("end_date" is not null, is it in the future?  If yes use it, else get next row
                                sEndDate = .Item("end_date")
                                'remove first "-"
                                sEndDate = sEndDate.Remove(4, 1)
                                'remove second "-"  - it goes from postion 7 in original to 6 after removal of first "-"
                                sEndDate = sEndDate.Remove(6, 1)
                                dEndDate = convToDateTime(sEndDate)
                                If Today < dEndDate Then
                                    dResult = .Item("CurrentRate")
                                    GoTo Terminate
                                Else
                                    dResult = Nothing
                                End If
                            End If
                        End If
                    Loop
                Else
                    dResult = Nothing
                    GoTo Terminate
                End If
            End With

        Catch ex As Exception
            writeToBatchLog(1, "Rating Group Tool (cFunctions): Could not get CurrentRate From " & sIPDataBase & " napower.providers_rates_index due to " & ex.Message)
            dResult = Nothing
        End Try
Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        GetCurrentRate_by_id = dResult


    End Function  ' end get current rate via rate index id

    ' Get CancelFee
    Public Shared Function GetCancelFee(ByVal sPlanCd As String) As String


        'This function will return a string  or nothing.  When a string  is returned we found the planType in IP
        Dim dResult As String = Nothing
        Dim sQry As String = Nothing
        Dim comm As OdbcCommand = Nothing
        Dim rs As OdbcDataReader = Nothing
        Dim sEndDate As String = Nothing
        Dim dEndDate As Date = Nothing

        Try
            sQry = " select ltrim(rtrim(convert(varchar(20),ip2.fixedCancellationFee))) As CancelFee, ip2.end_date from OPENQUERY(" &
                    sIPDataBase & ", 'select ip.fixedCancellationFee, cast(ip.planenddate as char) as end_date " &
                   " from napower.providers_rates_index ip where ltrim(rtrim(ip.planCode)) = """ & sPlanCd & """ ') ip2"


            comm = New OdbcCommand(sQry, dbconn)
            rs = comm.ExecuteReader()

            With rs
                If .HasRows Then
                    Do While .Read
                        If Not IsDBNull(.Item("CancelFee")) Then
                            'if the plan end date is null then use it, else find the null one.....loop
                            'this is needed because sometimes we reuse non fend  planCodes.
                            If .Item("end_date") = "0000-00-00" Or IsDBNull(.Item("end_date")) Then
                                dResult = .Item("CancelFee")
                                GoTo Terminate
                            Else
                                'if .Item("end_date" is not null, is it in the future?  If yes use it, else get next row
                                sEndDate = .Item("end_date")
                                'remove first "-"
                                sEndDate = sEndDate.Remove(4, 1)
                                'remove second "-"  - it goes from postion 7 in original to 6 after removal of first "-"
                                sEndDate = sEndDate.Remove(6, 1)
                                dEndDate = convToDateTime(sEndDate)
                                If Today < dEndDate Then
                                    dResult = .Item("CancelFee")
                                    GoTo Terminate
                                Else
                                    dResult = "0"
                                End If
                            End If
                        Else
                            dResult = "0"
                        End If
                    Loop
                Else
                    dResult = "0"
                    GoTo Terminate
                End If
            End With

        Catch ex As Exception
            writeToBatchLog(1, "Rating Group Tool (cFunctions): Could not get CancelFee From " & sIPDataBase & " napower.providers_rates_index due to " & ex.Message)
            dResult = Nothing
        End Try
Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        GetCancelFee = dResult


    End Function
    ' get Cancel fee via rate index id
    Public Shared Function GetCancelFee_by_id(ByVal iRateIndexId As Integer) As String


        'This function will return a string  or nothing.  When a string  is returned we found the planType in IP
        Dim dResult As String = Nothing
        Dim sQry As String = Nothing
        Dim comm As OdbcCommand = Nothing
        Dim rs As OdbcDataReader = Nothing
        Dim sEndDate As String = Nothing
        Dim dEndDate As Date = Nothing

        Try
            sQry = " select ltrim(rtrim(convert(varchar(20),ip2.fixedCancellationFee))) As CancelFee, ip2.end_date from OPENQUERY(" &
                      sIPDataBase & ", 'select ip.fixedCancellationFee, cast(ip.planenddate as char) as end_date " &
                     " from napower.providers_rates_index ip where ip.id = " & iRateIndexId & " ') ip2"


            comm = New OdbcCommand(sQry, dbconn)
            rs = comm.ExecuteReader()

            With rs
                If .HasRows Then
                    Do While .Read
                        If Not IsDBNull(.Item("CancelFee")) Then
                            'if the plan end date is null then use it, else find the null one.....loop
                            'this is needed because sometimes we reuse non fend  planCodes.
                            If .Item("end_date") = "0000-00-00" Or IsDBNull(.Item("end_date")) Then
                                dResult = .Item("CancelFee")
                                GoTo Terminate
                            Else
                                'if .Item("end_date" is not null, is it in the future?  If yes use it, else get next row
                                sEndDate = .Item("end_date")
                                'remove first "-"
                                sEndDate = sEndDate.Remove(4, 1)
                                'remove second "-"  - it goes from postion 7 in original to 6 after removal of first "-"
                                sEndDate = sEndDate.Remove(6, 1)
                                dEndDate = convToDateTime(sEndDate)
                                If Today < dEndDate Then
                                    dResult = .Item("CancelFee")
                                    GoTo Terminate
                                Else
                                    dResult = "0"
                                End If
                            End If
                        Else
                            dResult = "0"
                        End If
                    Loop
                Else
                    dResult = "0"
                    GoTo Terminate
                End If
            End With

        Catch ex As Exception
            writeToBatchLog(1, "Rating Group Tool (cFunctions): Could not get CancelFee From " & sIPDataBase & " napower.providers_rates_index due to " & ex.Message)
            dResult = Nothing
        End Try
Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        GetCancelFee_by_id = dResult


    End Function  ' end get Cancel Fee via rate index id

    Public Shared Function GetPlanTerm(ByVal sPlanCd As String) As Integer


        'This function will return a number or nothing when a number is returned we found the term from IP 

        Dim dResult As String = Nothing
        Dim sQry As String = Nothing
        Dim comm As OdbcCommand = Nothing
        Dim rs As OdbcDataReader = Nothing
        Dim sEndDate As String = Nothing
        Dim dEndDate As Date = Nothing

        Try
            sQry = "select ip2.nextPlanAfterNumMonth as  planTerm, ip2.end_date from OPENQUERY(" &
                    sIPDataBase & ", 'select ip.nextPlanAfterNumMonth, cast(ip.planenddate as char) as end_date " &
                   " from napower.providers_rates_index ip where ip.planCode  = """ & sPlanCd & """ ') ip2"


            comm = New OdbcCommand(sQry, dbconn)
            rs = comm.ExecuteReader()

            With rs
                If .HasRows Then
                    Do While .Read
                        If Not IsDBNull(.Item("planTerm")) Then
                            'if the plan end date is null then use it, else find the null one.....loop
                            'this is needed because sometimes we reuse non fend planCodes.
                            If .Item("end_date") = "0000-00-00" Or IsDBNull(.Item("end_date")) Then
                                dResult = .Item("planTerm")
                                GoTo Terminate
                            Else
                                sEndDate = .Item("end_date")
                                'remove first "-"
                                sEndDate = sEndDate.Remove(4, 1)
                                'remove second "-"  - it goes from postion 7 in original to 6 after removal of first "-"
                                sEndDate = sEndDate.Remove(6, 1)
                                dEndDate = convToDateTime(sEndDate)
                                'writeToBatchLog(0, "Rating Group Tool (cFunctions):  string date =  " & sEndDate & " converted date = " & dEndDate)
                                If Today < dEndDate Then
                                    dResult = .Item("planTerm")
                                    GoTo Terminate
                                Else
                                    dResult = Nothing
                                End If

                            End If
                        End If
                    Loop
                Else
                    dResult = Nothing
                    GoTo Terminate
                End If
            End With

        Catch ex As Exception
            dResult = Nothing
            writeToBatchLog(1, "Rating Group Tool (cFunctions): Could not get TERM From " & sIPDataBase & " napower.providers_rates_index due to " & ex.Message)
        End Try
Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        GetPlanTerm = dResult


    End Function
    Public Shared Function GetCustomerSurvey(ByVal oLDCAccount As RMAPI.LdcAccount) As Boolean


        'This function will return a True if the customer was found on the NAPOWER.CUSTOMERS_SURVEY from IP. If not found a false is returned 

        Dim dResult As Boolean = False
        Dim sQry As String = Nothing
        Dim comm As OdbcCommand = Nothing
        Dim rs As OdbcDataReader = Nothing

        Try
            sQry = "select ip2.CustomerId as  IPID from OPENQUERY(" &
                    sIPDataBase & ", 'select ip.CustomerId from customers_survey ip" &
                   " where ip.CustomerId  = """ & oLDCAccount.AlternateId & """ ') ip2"


            comm = New OdbcCommand(sQry, dbconn)
            rs = comm.ExecuteReader()

            With rs
                If .HasRows Then
                    Do While .Read
                        If Not IsDBNull(.Item("IPID")) Then
                            dResult = True
                            GoTo Terminate
                        Else
                            dResult = False
                            GoTo Terminate
                        End If
                    Loop
                Else
                    dResult = False
                    GoTo Terminate
                End If
            End With

        Catch ex As Exception
            dResult = False
            writeToBatchLog(1, "Rating Group Tool (cFunctions): Error Getting Data from  " & sIPDataBase & ".NAPOWER.CUSTOMERS_SURVEY due to " & ex.Message)
        End Try
Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        GetCustomerSurvey = dResult


    End Function

    Public Shared Function CreateUDF(ByVal oCustomer As RMAPI.Customer, ByVal oLDCAccount As RMAPI.LdcAccount, ByVal sValue As String) As Boolean
        'Search for UDFdefinitionID
        Dim bsFieldDefinitionID As String = ""
        Dim colUserFieldDefs As CoreAPI.UserFieldDefinitionList
        Dim oUserFieldDef As CoreAPI.UserFieldDefinition
        Dim colUDFs As CoreAPI.UserFieldDataList
        Dim oUDF As CoreAPI.UserFieldData
        Dim bProcessed As Boolean = False
        Dim bUpdatedUDF As Boolean = False
        Dim bCreatedUDF As Boolean = False
        Dim iCount As Integer = 0
        Dim iItem As Integer = 0



        'IT-573 Start - deleted  old If statement and added Case Statement 
        ' 

        Select Case sValue
            Case Is = "REFBN"
                'create Meter UDF "METER_MIUP_CHANGE_TYPE" with value "REFBN"
                Dim oMeter As RMAPI.Meter
                Dim oMeters As RMAPI.MeterList
                oMeters = oLDCAccount.GetMeters
                If oMeters.Count = 0 Then
                    writeToBatchLog(2, "Rating Group Tool (cFunctions): No meter exists for LDC account # " & oLDCAccount.LDCAcctNo, oLDCAccount)
                    Return False
                End If

                'cycle through all meters on LDC Account
                For Each oMeter In oMeters
                    bProcessed = False
                    Try
                        'Check if UDF exists, else create one
                        colUDFs = oMeter.GetUserFieldDataList("METER_MIUP_CHANGE_TYPE")
                        If colUDFs.Count > 1 Then
                            'Found more than one matching UDF
                            writeToBatchLog(1, "Rating Group Tool (cFunctions): Found more than one 'METER_MIUP_CHANGE_TYPE' UDF for meter '" & oMeter.DisplayName & "' on LDC account # " & oLDCAccount.LDCAcctNo, oLDCAccount)
                            Return False
                        End If

                        'Check if value matches that of LDC Vendor UDF, else change and save
                        If colUDFs.Count = 1 Then
                            oUDF = colUDFs.Item(0)
                            If oUDF.Value <> sValue Then
                                oUDF.Value = sValue
                                'TMcLane_0414 start
                                'oUDF.Save()
                                Try
                                    oUDF.Save()
                                Catch ex As Exception
                                    writeToBatchLog(2, ex.Message)
                                End Try
                                'TMcLane_0414 End
                                writeToBatchLog(0, "Rating Group Tool (cFunctions): Updated existing 'METER_MIUP_CHANGE_TYPE' UDF with value '" & sValue & "' for meter '" & oMeter.DisplayName & "' on LDC account # " & oLDCAccount.LDCAcctNo, oLDCAccount)
                            End If
                            bProcessed = True
                        End If

                    Catch ex As Exception
                        'UDF does not exist on meter.  Continue
                    End Try

                    'If no UDF is found, create one
                    If Not bProcessed Then
                        colUserFieldDefs = oMeter.GetUserFieldDefinitions()
                        For Each oUserFieldDef In colUserFieldDefs
                            If Trim(oUserFieldDef.DisplayName) = Trim("METER_MIUP_CHANGE_TYPE") Then
                                bsFieldDefinitionID = oUserFieldDef.DisplayCode()
                            End If
                        Next

                        oUDF = oMeter.GetNewUserFieldData(bsFieldDefinitionID)
                        oUDF.StartDate = oLDCAccount.StartDate
                        oUDF.Value = sValue
                        Try
                            oCustomer.Save()
                        Catch ex As Exception
                            writeToBatchLog(2, "Rating Group Tool (cFunctions): Could not Add UDF METER_MIUP_CHANGE_TYPE to LDC Account due to: " & ex.Message, oLDCAccount)
                            Return False
                        End Try

                        writeToBatchLog(0, " Rating Group Tool (cFunctions): Created 'METER_MIUP_CHANGE_TYPE' UDF with value '" & sValue & "' for meter '" & oMeter.DisplayName & "' on LDC account # " & oLDCAccount.LDCAcctNo, oLDCAccount)

                    End If
                Next
            Case Is = "RGA_NEEDED"
                colUserFieldDefs = oLDCAccount.GetUserFieldDefinitions()
                For Each oUserFieldDef In colUserFieldDefs
                    If Trim(oUserFieldDef.DisplayName) = Trim(sValue) Then
                        bsFieldDefinitionID = oUserFieldDef.DisplayCode()
                    End If
                Next

                oUDF = oLDCAccount.GetNewUserFieldData(bsFieldDefinitionID)
                oUDF.Value = "Y"

                Try
                    oCustomer.Save()
                Catch ex As Exception
                    writeToBatchLog(2, "Rating Group Tool (cFunctions): Could not Add UDF - " & sValue & "due to the following reason:" & ex.Message)
                    Return False
                End Try

                ' IT-RMUpGrade start
                ' Accelerated_RGA_NEEDED
            Case Is = "Accelerated_RGA_NEEDED"
                colUserFieldDefs = oLDCAccount.GetUserFieldDefinitions()
                For Each oUserFieldDef In colUserFieldDefs
                    If Trim(oUserFieldDef.DisplayName) = Trim(sValue) Then
                        bsFieldDefinitionID = oUserFieldDef.DisplayCode()
                    End If
                Next

                oUDF = oLDCAccount.GetNewUserFieldData(bsFieldDefinitionID)
                oUDF.Value = "Y"

                Try
                    oCustomer.Save()
                Catch ex As Exception
                    writeToBatchLog(2, "Rating Group Tool (cFunctions): Could not Add UDF - " & sValue & "due to the following reason:" & ex.Message)
                    Return False
                End Try
                ' IT-RMUpGrade end

            Case Is = "PURA_VAR_ALLOWED_IN"
                colUserFieldDefs = oLDCAccount.GetUserFieldDefinitions()
                For Each oUserFieldDef In colUserFieldDefs
                    If Trim(oUserFieldDef.DisplayName) = Trim(sValue) Then
                        bsFieldDefinitionID = oUserFieldDef.DisplayCode()
                    End If
                Next

                oUDF = oLDCAccount.GetNewUserFieldData(bsFieldDefinitionID)
                oUDF.Value = "Y"

                Try
                    oCustomer.Save()
                Catch ex As Exception
                    writeToBatchLog(2, "Rating Group Tool (cFunctions): Could not Add UDF - " & sValue & "due to the following reason:" & ex.Message)
                    Return False
                End Try


            Case Else  'Must be an MIUP type
                Try
                    colUDFs = oLDCAccount.GetUserFieldDataList("MIUP_CHANGE_TYPE")
                    If colUDFs.Count = 0 Then
                        bProcessed = False
                    Else
                        If colUDFs.Count > 1 Then
                            'Found more than one matching UDF
                            iCount = colUDFs.Count
                            iItem = (iCount - 1) 'The item number is 1 less than the count. So if the count is 3 the items are  0, 1, and 2
                            Do While iCount > 0
                                oUDF = colUDFs.Item(iItem)
                                If oUDF.Value = sValue Then
                                    'We found a valid MIUP that matches our INPUT Type. Set bProcessed to True and get out
                                    bProcessed = True
                                    Exit Do
                                Else
                                    'Found a non sValue MIUP so we may need to create one in the next section. Set bProcessed = False, decrement iCount and iItem and loop again
                                    bProcessed = False
                                    iCount = iCount - 1
                                    iItem = iItem - 1
                                End If
                            Loop
                        Else
                            'Count must be 1. Is it the correct MIUP? If yes do nothing. If no, create the sValue
                            oUDF = colUDFs.Item(0)
                            If oUDF.Value = sValue Then
                                bProcessed = True
                            Else
                                'Found a non sValue MIUP so we need to create one in the next section. Set bProcessed = False
                                bProcessed = False
                            End If
                        End If
                    End If
                Catch ex As Exception
                    'UDF does not exist.  Continue
                    bProcessed = False
                End Try

                'If no UDF is found, create one
                If bProcessed = False Then
                    colUserFieldDefs = oLDCAccount.GetUserFieldDefinitions()
                    For Each oUserFieldDef In colUserFieldDefs
                        If Trim(oUserFieldDef.DisplayName) = Trim("MIUP_CHANGE_TYPE") Then
                            bsFieldDefinitionID = oUserFieldDef.DisplayCode()
                        End If
                    Next

                    oUDF = oLDCAccount.GetNewUserFieldData(bsFieldDefinitionID)
                    oUDF.StartDate = oLDCAccount.StartDate
                    oUDF.Value = sValue

                    Try
                        oCustomer.Save()
                        ' writeToBatchLog(0, "Rating Group Tool (cFunctions): Created 'MIUP_CHANGE_TYPE' UDF with value '" & sValue & "' for LDC account # " & oLDCAccount.LDCAcctNo, oLDCAccount)

                    Catch ex As Exception
                        writeToBatchLog(2, "Rating Group Tool (cFunctions): " & ex.Message, oLDCAccount)
                        Return False
                    End Try

                End If
        End Select
        'IT-573 end

        Return True

    End Function 'CreateUDF

    'Start TM20170609
    Public Shared Function NewCreateUDF(ByVal oCustomer As RMAPI.Customer, ByVal oLDCAccount As RMAPI.LdcAccount, ByVal sUDFName As String, ByVal sUDFValue As String) As Boolean

        Dim bsFieldDefinitionID As String = ""
        Dim colUserFieldDefs As CoreAPI.UserFieldDefinitionList
        Dim oUserFieldDef As CoreAPI.UserFieldDefinition
        Dim oUDF As CoreAPI.UserFieldData

        ' 

        Select Case sUDFName

            Case Is = "RGA_INITIAL_RGID"
                colUserFieldDefs = oLDCAccount.GetUserFieldDefinitions()
                For Each oUserFieldDef In colUserFieldDefs
                    If Trim(oUserFieldDef.DisplayName) = Trim(sUDFName) Then
                        bsFieldDefinitionID = oUserFieldDef.DisplayCode()
                    End If
                Next

                oUDF = oLDCAccount.GetNewUserFieldData(bsFieldDefinitionID)
                oUDF.Value = sUDFValue

                Try
                    oCustomer.Save()
                Catch ex As Exception
                    writeToBatchLog(2, "Rating Group Tool (cFunctions): Could not Add UDF - " & sUDFName & "due to the following reason:" & ex.Message)
                    Return False
                End Try


            Case Else
                'UDF does not exist.  Continue
                Return False

        End Select


        Return True

    End Function 'NeWCreateUDF  --  End  TM20170609



    Public Shared Function CountUDF(ByVal oObject, ByVal sUDFName) As Integer

        Dim colUDFs As CoreAPI.UserFieldDataList = Nothing

        colUDFs = oObject.GetUserFieldDataList(sUDFName)

        Return colUDFs.Count

    End Function


    Public Shared Function GetUDFValue(ByVal oObject, ByVal sUDFName) As Integer

        Dim oUDF As CoreAPI.UserFieldData

        Try
            oUDF = oObject.GetUserFieldDataList(sUDFName).Item(0)
            Return CInt(oUDF.Value)

        Catch ex As Exception
            'UDF does not exist.  Continue
            Return 0
        End Try

    End Function
    Public Shared Function GetUDFValue_S(ByVal oLDCAccount As RMAPI.LdcAccount, ByVal sUDFName As String) As String

        Dim oUDF As CoreAPI.UserFieldData

        Try
            oUDF = oLDCAccount.GetUserFieldDataList(sUDFName).Item(0)
            Return CStr(oUDF.Value)

        Catch ex As Exception
            'UDF does not exist.  Continue
            Return Nothing
        End Try

    End Function
    Public Shared Function UpdateUDFValue(ByVal oCustomer As RMAPI.Customer, ByVal oLDCAccount As RMAPI.LdcAccount, ByVal sUDFName As String, ByVal sValue As String) As Boolean

        Dim oUDF As CoreAPI.UserFieldData

        Try
            oUDF = oLDCAccount.GetUserFieldDataList(sUDFName).Item(0)
            oUDF.Value = sValue
            oUDF.Save()
            Return True
        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): Error on upating UDF " & sUDFName & " for LDC account " & ex.Message, oLDCAccount)
            Return False
        End Try

    End Function

    'This function gets the UDF for the LDC account it was passed and the by the sValue it is passed sValue
    Public Shared Function RemoveUDF(ByVal oCustomer As RMAPI.Customer, ByVal oLDCAccount As RMAPI.LdcAccount, ByVal sUDFName As String) As Boolean

        Dim colUDFs As CoreAPI.UserFieldDataList
        Dim oUDF As CoreAPI.UserFieldData
        Dim bProcessed As Boolean = False
        Dim iCount As Integer = 0
        Dim iItem As Integer = 0

        'Check if UDF exists, if yes whack it....should only be one most of the time but remove all if more than 1
        Try
            colUDFs = oLDCAccount.GetUserFieldDataList(sUDFName)
            If colUDFs.Count = 0 Then
                bProcessed = False
            Else
                'Found one or more matching UDF
                iCount = colUDFs.Count
                iItem = (iCount - 1) 'The item number is 1 less than the count. So if the count is 3 the items are  0, 1, and 2
                Do While iCount > 0
                    oUDF = colUDFs.Item(iItem)
                    oUDF.Remove()
                    oCustomer.Save()
                    bProcessed = True
                    iCount = iCount - 1
                    iItem = iItem - 1
                Loop
            End If

        Catch ex As Exception
            'UDF does not exist.  Continue
            bProcessed = False
        End Try


        If bProcessed = True Then
            Return True
        Else
            Return False
        End If

    End Function 'RemoveUDF
    Public Shared Function SetPURAUDFs(ByVal oCust As RMAPI.Customer, ByVal oAccount As RMAPI.Account,
                              ByVal oVendor As RMAPI.Vendor, ByVal oLDC As RMAPI.LdcAccount, ByVal sPlanType As String, ByVal ntermInMonths As Short, ByVal sEndDate As String,
                              ByVal nCancelFee As Short, ByVal sNextRate As String, ByVal sMIUP As String) As Boolean

        'Array Constants
        Dim aPURA_UDFNAME = New String() {"Cancellation Fee", "Variable Rate", "ESP RATE TERM", "RATE EXPIRATION DATE", "NEXT CYCLE ESP RATE"}
        Dim aPURA_MIUP_Types = New String() {"AMTEN", "REFPR", "REFTC", "DTM036", "REFPL"}  'These correspond to the UDF name above. 

        'Array Variables
        Dim aPURA_UDF_Value(5) As String

        'function variables
        Dim nUDFCount As Short
        Dim nMaxLoop As Short = 4 'Number of PURA UDF's is 0 to 4
        Dim sCurrentValue As String
        Dim bUpdateDone As Boolean = False

        Try
            aPURA_UDF_Value(0) = CStr(nCancelFee)
            Select Case sPlanType
                Case Is = "var"
                    aPURA_UDF_Value(1) = "V"
                Case Is = "stepup"
                    aPURA_UDF_Value(1) = "V"
                Case Else
                    aPURA_UDF_Value(1) = "NV"
            End Select
            aPURA_UDF_Value(2) = CStr(ntermInMonths)
            aPURA_UDF_Value(3) = sEndDate
            aPURA_UDF_Value(4) = sNextRate


            For i = 0 To nMaxLoop Step +1
                Try
                    nUDFCount = CountUDF(oLDC, aPURA_UDFNAME(i))
                    If nUDFCount = 0 Then
                        CreatePURAUDF(oCust, oLDC, aPURA_UDFNAME(i), aPURA_UDF_Value(i))
                        bUpdateDone = True
                    Else
                        sCurrentValue = LTrim(GetUDFValue_S(oLDC, aPURA_UDFNAME(i)))
                        If sCurrentValue = Nothing Or LTrim(sCurrentValue) <> LTrim(aPURA_UDF_Value(i)) Then
                            UpdateUDFValue(oCust, oLDC, aPURA_UDFNAME(i), aPURA_UDF_Value(i))
                            bUpdateDone = True
                        End If
                    End If
                Catch ex As Exception
                    writeToBatchLog(2, "Rating Group Tool (cFunctions): Could not Set UDF Values of " & aPURA_UDFNAME(i) & " due to the following reason:" & ex.Message & " For LDC ACCT NO = " & oLDC.LDCAcctNo, oLDC)
                    Return False
                End Try

                Try
                    'Function CreateUDF checks for the MIUP_CHANGE_TYPE of "AMTEN", "REFPR", "REFTC", "DTM036",  or "REFPL" before creating them
                    If bUpdateDone = True Then
                        CreateUDF(oCust, oLDC, aPURA_MIUP_Types(i))
                    End If
                Catch ex As Exception
                    writeToBatchLog(2, "Rating Group Tool (cFunctions): Could not Set MIUP Type  " & aPURA_MIUP_Types(i) & " due to the following reason:" & ex.Message & " For LDC ACCT NO = " & oLDC.LDCAcctNo, oLDC)
                    Return False
                End Try
            Next
            Return True

        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): Could not Set UDF Values  due to the following reason:" & ex.Message & " For LDC ACCT NO = " & oLDC.LDCAcctNo, oLDC)
            Return False
        End Try
    End Function 'Set PURA UDF
    Public Shared Function CreatePURAUDF(ByVal oCustomer As RMAPI.Customer, ByVal oLDCAccount As RMAPI.LdcAccount, ByVal sUDFName As String, sUDFValue As String) As Boolean

        Dim bsFieldDefinitionID As String = ""
        Dim colUserFieldDefs As CoreAPI.UserFieldDefinitionList
        Dim oUserFieldDef As CoreAPI.UserFieldDefinition
        Dim oUDF As CoreAPI.UserFieldData

        'Search for UDFdefinitionID
        colUserFieldDefs = oLDCAccount.GetUserFieldDefinitions()
        For Each oUserFieldDef In colUserFieldDefs
            If Trim(oUserFieldDef.DisplayName) = Trim(sUDFName) Then
                bsFieldDefinitionID = oUserFieldDef.DisplayCode()
            End If
        Next

        oUDF = oLDCAccount.GetNewUserFieldData(bsFieldDefinitionID)
        oUDF.Value = sUDFValue

        Try
            oCustomer.Save()
        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): Could not Add PURA UDF - " & sUDFName & "due to the following reason:" & ex.Message)
            Return False
        End Try

        Return True

    End Function 'CreatePURAUDF
    Public Shared Function CreateUDF_WithDates(ByVal oCustomer As RMAPI.Customer, ByVal oLDCAccount As RMAPI.LdcAccount, ByVal sUDFName As String, ByVal sUDFValue As String, ByVal dStartDate As Date, ByVal dEndDate As Date) As Boolean

        Dim bsFieldDefinitionID As String = ""
        Dim colUserFieldDefs As CoreAPI.UserFieldDefinitionList
        Dim oUserFieldDef As CoreAPI.UserFieldDefinition
        Dim oUDF As CoreAPI.UserFieldData


        Try

            'Search for UDFdefinitionID
            colUserFieldDefs = oLDCAccount.GetUserFieldDefinitions()
            For Each oUserFieldDef In colUserFieldDefs
                If Trim(oUserFieldDef.DisplayName) = Trim(sUDFName) Then
                    bsFieldDefinitionID = oUserFieldDef.DisplayCode()
                End If
            Next

            oUDF = oLDCAccount.GetNewUserFieldData(bsFieldDefinitionID)
            oUDF.Value = sUDFValue
            oUDF.StartDate = dStartDate
            If dEndDate <> Nothing Then
                oUDF.EndDate = dEndDate
            End If
            oUDF.EndDate = dEndDate
            oCustomer.Save()
        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): Could not Add  - " & sUDFName & "due to the following reason:" & ex.Message)
            Return False
        End Try

        Return True

    End Function
    'New Function to UpDateUDF with dates
    Public Shared Function UpDateUDF_WithDates(ByVal oCustomer As RMAPI.Customer, ByVal oLDCAccount As RMAPI.LdcAccount, ByVal sUDFName As String, ByVal sUDFValue As String, ByVal dStartDate As Date, ByVal dEndDate As Date) As Boolean


        Dim oUDF As CoreAPI.UserFieldData

        Try
            oUDF = oLDCAccount.GetUserFieldDataList(sUDFName).Item(0)
            If sUDFValue <> Nothing Then
                oUDF.Value = sUDFValue
            End If

            If dStartDate <> Nothing Then
                oUDF.StartDate = dStartDate
            End If

            If dEndDate <> Nothing Then
                oUDF.EndDate = dEndDate
            End If

            oUDF.Save()
            Return True

        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): Could not Update  - " & sUDFName & "due to the following reason:" & ex.Message)
            Return False
        End Try

    End Function ' UpDateUDF_WithDates

    Public Shared Function CreateInteraction(ByVal oICustomer As RMAPI.Customer, ByVal oLDCAccount As RMAPI.LdcAccount, ByVal sMessage As String, ByVal sSubType As String) As Boolean
        Dim oNewInter As CoreAPI.Interaction

        oNewInter = oICustomer.GetNewInteraction()
        oNewInter.Date = Today
        oNewInter.Text = sMessage
        oNewInter.RelatedClassName = "cLDCAccount"
        oNewInter.RelatedId = oLDCAccount.ObjectId
        oNewInter.TypeCode = "EV"
        oNewInter.SubTypeCode = sSubType
        Try
            oNewInter.Save()
            Return True
        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): Error on creating Interaction for LDC account " & ex.Message, oLDCAccount)
            writeToBatchLog(2, sMessage, oLDCAccount)
            Return False
        End Try ' saving new inteactions




    End Function 'Create Interaction
    Public Shared Function CreateEBT(ByVal oCust As RMAPI.Customer, ByVal oAccount As RMAPI.Account,
                              ByVal oVendor As RMAPI.Vendor, ByVal oLDC As RMAPI.LdcAccount, ByVal oRG As RMAPI.RatingGroup, ByVal sEBTType As String) As Boolean

        'String Variables
        Dim sRateChangeType As String = ""
        Dim sTransSubType As String = "REFRB"
        Dim sIMessage As String = " "
        Dim sISubType As String = " "
        Dim sPlanCodeMessage As String = " "

        'boolean variables
        Dim bSendCr As Boolean = False

        'Numeric Variables
        Dim nCRSendDaysPrior As Short = 0
        Dim nIPRecessionDays As Short = 0
        Dim nDaysDiff As Integer = 0
        'date variables
        Dim dSendate As Date = Nothing

        Try
            'Find Days prior UDF
            Select Case CountUDF(oVendor, "CRSendDaysPrior")
                Case Is = 0
                    writeToBatchLog(0, " Rating Group Tool (cFunctions): Missing 'CRSendDaysPrior' UDF for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                    bSendCr = False
                Case Is = 1
                    Try
                        nCRSendDaysPrior = CShort(RMAPI.Vendor.GetVendor(oLDC.VendorDBNo).GetUserFieldDataByName("CRSendDaysPrior", Now))
                        bSendCr = True
                    Catch ex As Exception
                        writeToBatchLog(0, " Rating Group Tool (cFunctions): Exception in processing 'CRSendDaysPrior' UDF for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                        bSendCr = False
                    End Try
                Case Is > 1
                    writeToBatchLog(2, " Rating Group Tool (cFunctions): More than one 'CRSendDaysPrior' UDF defined for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                    Return False
            End Select

            If sEBTType = "NEWRATEINDEXID_RATECH" Then
                ' Get IP_RECESSION DAYS 
                Select Case CountUDF(oVendor, "IP_RECESSION_DAYS")
                    Case Is = 0
                        writeToBatchLog(0, " Rating Group Tool (cFunctions): Missing 'IP_RECESSION_DAYS' UDF for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                        nIPRecessionDays = 0
                    Case Is = 1
                        Try
                            nIPRecessionDays = CShort(RMAPI.Vendor.GetVendor(oLDC.VendorDBNo).GetUserFieldDataByName("IP_RECESSION_DAYS", Now))
                        Catch ex As Exception
                            writeToBatchLog(0, " Rating Group Tool (cFunctions): Exception in  'IP RecessionDays' UDF for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                            nIPRecessionDays = 0
                        End Try
                    Case Is > 1
                        writeToBatchLog(2, " Rating Group Tool (cFunctions): More than one 'IP_RECESSION_DAYS' UDF defined for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                        Return False
                End Select
            Else
                nIPRecessionDays = 0
            End If

            If bSendCr = True Then
                'Check if Vendor has the "RATE CHANGE CODE" UDF defined:
                ' - If True: create EBT Misc Change Request (MIUP)
                ' - If False: create EBT Rate Change Request (REFRB)
                Select Case CountUDF(oVendor, "RATE CHANGE CODE")
                    Case Is = 0
                        'Only write error messsage if one of the following Vendors
                        'Orange and Rockland ------------------ 81
                        'Orange and Rockland - GAS------------- 343
                        'ConEd--------------------------------- 77
                        'ConEd- GAS---------------------------- 344
                        'New Jersey Natural Gas Company---------281
                        'South Jersey Gas Company-------------- 280
                        'Rockland Electric--------------------- 535

                        If oLDC.VendorId = 81 Or oLDC.VendorId = 343 Or oLDC.VendorId = 77 Or
                            oLDC.VendorId = 344 Or oLDC.VendorId = 281 Or oLDC.VendorId = 280 Or
                            oLDC.VendorId = 535 Then
                            writeToBatchLog(2, " Rating Group Tool (cFunctions): Missing 'RATE CHANGE CODE' UDF for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                            Return False
                        End If
                    Case Is = 1
                        'Get the MIUP type needed later on
                        sRateChangeType = RMAPI.Vendor.GetVendor(oLDC.VendorDBNo).GetUserFieldDataByName("RATE CHANGE CODE", Now)
                        'writeToBatchLog(0, "Rating Group Tool (cFunctions): sRateChangeType = " & sRateChangeType, oLDC)
                    Case Is > 1
                        writeToBatchLog(2, "Rating Group Tool (cFunctions): More than one 'RATE CHANGE CODE' UDF defined for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                        Return False
                End Select

                ' IT-1408 - Send out a Rate Change Request  but before we do determine the  sub type and create the proper MIUP change type if needed. 
                Try
                    'sRateChangeType was determined by the oVendor UDF "RATE CHANGE CODE"
                    'If Null the vendor uses a REFRB
                    If sRateChangeType <> "" Then
                        sTransSubType = "MIUP"
                        'Count the number of UDF  MIUP_CHANGE_TYPE. 
                        '---If 0 add the correct one for the vendor
                        '---If one verfiy that it is the correct one - if the correct one do nothing, if the wrong one update to the correct one
                        '---if two do nothing , it is present
                        Select Case CountUDF(oLDC, "MIUP_CHANGE_TYPE")
                            Case Is = 0
                                'If Transaction Sub Type = "MIUP" create the MIUP_CHANGE_TYPE UDF with the value from the Vendor UDF "RATE_CHANGE_CODE if one does not exist - CreatUDF checks before creating"
                                If CreateUDF(oCust, oLDC, sRateChangeType) <> True Then
                                    writeToBatchLog(2, "Rating Group Tool (cFunctions): Error on creating MIUP Change Type UDF for LDC account # " & oLDC.LDCAcctNo, oLDC)
                                    Return False
                                End If
                            Case Is = 1
                                If sRateChangeType = GetUDFValue_S(oLDC, sRateChangeType) Then
                                    'do nothing we have a UDF
                                Else
                                    If UpdateUDFValue(oCust, oLDC, "MIUP_CHANGE_TYPE", sRateChangeType) <> True Then
                                        writeToBatchLog(2, "Rating Group Tool (cFunctions): Error on updating MIUP Change Type UDF for LDC account # " & oLDC.LDCAcctNo, oLDC)
                                        Return False
                                    End If
                                End If
                                'Case Is = 2
                                '   If RemoveUDF(oCust, oLDC, "AMT9M") <> True Then
                                'writeToBatchLog(2, "Rating Group Tool (cFunctions): Error on Removing  MIUP UDF AMT9M  for LDC account # " & oLDC.LDCAcctNo, oLDC)
                                'Return False
                                'Else
                                'If sRateChangeType = GetUDFValue_S(oLDC, sRateChangeType) Then
                                'do nothing we have a UDF with the proper sRateChangeType
                                'Else
                                'writeToBatchLog(2, "Rating Group Tool (cFunctions): Error Cannot find UDF MIUP for " & sRateChangeType & "  for LDC account # " & oLDC.LDCAcctNo, oLDC)
                                'Return False
                                'End If
                                'End If
                            Case Else
                                'do nothing
                        End Select
                        'writeToBatchLog(0, "Rating Group Tool (cFunctions): sRateChangeType = " & sRateChangeType, oLDC)
                    Else
                        'This vendor gets a REFRB  
                        sTransSubType = "REFRB"
                    End If
                    'end IT-1408


                    If nIPRecessionDays > 0 Then
                        If oRG.StartDate.AddDays(nCRSendDaysPrior) < Today.AddDays(nIPRecessionDays) Then
                            nDaysDiff = DateDiff("d", oRG.StartDate.AddDays(nCRSendDaysPrior), Today.AddDays(nIPRecessionDays))
                            nCRSendDaysPrior = nCRSendDaysPrior + nDaysDiff
                            'writeToBatchLog(0, "Rating Group Tool (cFunctions): Days Different =  " & nDaysDiff & " New CRSendDayPrior = " & nCRSendDaysPrior)
                        End If
                    End If

                    oLDC.CreateEBTChangeRequest(sTransSubType, oAccount.VendorDBNo, oRG.StartDate, Nothing, oRG.StartDate.AddDays(nCRSendDaysPrior))
                    Return True

                Catch ex As Exception
                    writeToBatchLog(2, "Rating Group Tool (cFunctions): Failed to create rate Change Request for for LDC account # " & oLDC.LDCAcctNo & ex.Message, oLDC)
                    Return False
                End Try
            Else
                Return False
            End If

        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): A general exception occurred when processing EBT Transactions for LDC account # " & oLDC.LDCAcctNo & " | Error message: " & ex.Message, oLDC)
            Return False
        End Try
        Return True
    End Function
    Public Shared Function CreateEBT_REFRB_Explict_Dates(ByVal oCust As RMAPI.Customer, ByVal oAccount As RMAPI.Account, ByVal oVendor As RMAPI.Vendor, ByVal oLDC As RMAPI.LdcAccount, ByVal dReqDate As Date, ByVal dSendDate As Date,
                                                   ByVal sEBTType As String) As Boolean

        'String Variables
        Dim sRateChangeType As String = ""
        Dim sTransSubType As String = "REFRB"
        Dim sIMessage As String = " "
        Dim sISubType As String = " "
        Dim sPlanCodeMessage As String = " "

        'boolean variables
        Dim bSendCr As Boolean = False



        Try
            oLDC.CreateEBTChangeRequest(sTransSubType, oAccount.VendorDBNo, dReqDate, Nothing, dSendDate)
            Return True

        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): Failed to create rate Change Request for for LDC account # " & oLDC.LDCAcctNo & ex.Message, oLDC)
            Return False
        End Try

        Return True
    End Function
    Public Shared Function CreateEBT_MIUP_Explict_Dates(ByVal oCust As RMAPI.Customer, ByVal oAccount As RMAPI.Account, ByVal oVendor As RMAPI.Vendor, ByVal oLDC As RMAPI.LdcAccount, ByVal dReqDate As Date, ByVal dSendDate As Date
                                                  ) As Boolean

        'String Variables
        Dim sTransSubType As String = "MIUP"


        Try
            oLDC.CreateEBTChangeRequest(sTransSubType, oAccount.VendorDBNo, dReqDate, Nothing, dSendDate)
            Return True

        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): Failed to create MIUP Change Request for for LDC account # " & oLDC.LDCAcctNo & ex.Message, oLDC)
            Return False
        End Try

        Return True
    End Function
    Public Shared Function CreatePURAEBT(ByVal oCust As RMAPI.Customer, ByVal oAccount As RMAPI.Account,
                            ByVal oVendor As RMAPI.Vendor, ByVal oLDC As RMAPI.LdcAccount, ByVal dDate As Date, ByVal sSubType As String, ByVal bInitiateMIUP As Boolean) As Boolean

        '


        'Numeric Variables
        Dim nCRSendDaysPrior As Short = 0
        Dim nIPRecessionDays As Short = 0
        Dim nDaysDiff As Integer = 0
        Dim nDaysDiffREQ As Integer = 0

        'date variables
        Dim dSendate As Date = Nothing

        Try
            'Find Days prior UDF
            Select Case CountUDF(oVendor, "CRSendDaysPrior")
                Case Is = 0
                    writeToBatchLog(0, " Rating Group Tool (cFunctions): Missing 'CRSendDaysPrior' UDF for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)

                Case Is = 1
                    Try
                        nCRSendDaysPrior = CShort(RMAPI.Vendor.GetVendor(oLDC.VendorDBNo).GetUserFieldDataByName("CRSendDaysPrior", Now))

                    Catch ex As Exception
                        writeToBatchLog(0, " Rating Group Tool (cFunctions): Exception in processing 'CRSendDaysPrior' UDF for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                        Return False
                    End Try
                Case Is > 1
                    writeToBatchLog(2, " Rating Group Tool (cFunctions): More than one 'CRSendDaysPrior' UDF defined for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                    Return False
            End Select

            If sSubType = "REFRB" Then
                ' Get IP_RECESSION DAYS 
                Select Case CountUDF(oVendor, "IP_RECESSION_DAYS")
                    Case Is = 0
                        writeToBatchLog(0, " Rating Group Tool (cFunctions): Missing 'IP_RECESSION_DAYS' UDF for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                        nIPRecessionDays = 0
                    Case Is = 1
                        Try
                            nIPRecessionDays = CShort(RMAPI.Vendor.GetVendor(oLDC.VendorDBNo).GetUserFieldDataByName("IP_RECESSION_DAYS", Now))
                        Catch ex As Exception
                            writeToBatchLog(0, " Rating Group Tool (cFunctions): Exception in  'IP RecessionDays' UDF for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                            nIPRecessionDays = 0
                        End Try
                    Case Is > 1
                        writeToBatchLog(2, " Rating Group Tool (cFunctions): More than one 'IP_RECESSION_DAYS' UDF defined for LDC Vendor # '" & oLDC.VendorDBNo & "'", oLDC)
                        Return False
                End Select
            Else
                nIPRecessionDays = 0
            End If


            Try
                nDaysDiffREQ = 0
                'if this is the initatePURA MIUP then send it out today plus 1 by making nCRSendDaysPrior = 0 therefore setting send date = today
                If bInitiateMIUP = True Then
                    nCRSendDaysPrior = 0
                    nIPRecessionDays = 0
                    'if an Initiate PURA MIUP and ENRL and LDC start date in the future -  make the request date equal to the LDC start date to match the UDF start dates
                    If oLDC.Status = "ENRL" And oLDC.StartDate > Today Then
                        nDaysDiffREQ = DateDiff("d", Today, oLDC.StartDate)
                    Else
                        nDaysDiffREQ = 0
                    End If
                Else
                    'if not a Initiate PURA MIUP is it a ENRL MIUP with a start date in the future then make the request date equal to the LDC start date to match the UDF start dates
                    If oLDC.StartDate > Today And oLDC.Status = "ENRL" And sSubType = "MIUP" Then
                        If oLDC.VendorId = 78 Then
                            ' If CL&P then make add 5 days to the LDC start date to get the nDaysDiffREQ and  make the nCRSendDaysPrior equal to nDaysDiffReq --  
                            'so the MIUP does not go out before or on the LDC start date --- CL&P rejects these
                            nDaysDiffREQ = DateDiff("d", Today, oLDC.StartDate.AddDays(+5))
                            If nDaysDiffREQ > nCRSendDaysPrior Then
                                nCRSendDaysPrior = nDaysDiffREQ
                            End If
                        Else
                            nDaysDiffREQ = DateDiff("d", Today, oLDC.StartDate)
                        End If
                    Else
                        nDaysDiffREQ = 0
                    End If
                End If

                If nIPRecessionDays > 0 Then
                    If dDate.AddDays(nCRSendDaysPrior) < Today.AddDays(nIPRecessionDays) Then
                        nDaysDiff = DateDiff("d", dDate.AddDays(nCRSendDaysPrior), Today.AddDays(nIPRecessionDays))
                        nCRSendDaysPrior = nCRSendDaysPrior + nDaysDiff
                    End If
                End If
                'nDaysDiffREQ will be greater than 0 if the status is ENRL and the LDC start date is in the future
                oLDC.CreateEBTChangeRequest(sSubType, oAccount.VendorDBNo, dDate.AddDays(nDaysDiffREQ), Nothing, dDate.AddDays(nCRSendDaysPrior))
                Return True

            Catch ex As Exception
                writeToBatchLog(2, "Rating Group Tool (cFunctions): Failed to create rate Change Request for for LDC account # " & oLDC.LDCAcctNo & ex.Message, oLDC)
                Return False
            End Try

        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): A general exception occurred when processing EBT Transactions for LDC account # " & oLDC.LDCAcctNo & " | Error message: " & ex.Message, oLDC)
            Return False
        End Try

        Return True

    End Function   'CreatePURAEBT
    'HE-24 start
    Public Shared Function CreateHREBT(ByVal oCust As RMAPI.Customer, ByVal oAccount As RMAPI.Account, ByVal oVendor As RMAPI.Vendor, ByVal oLDC As RMAPI.LdcAccount) As Boolean

        Dim dSendDate As Date = Nothing
        Dim dFirstCycleDate As Date = Nothing
        Dim iDayofWeek As Integer = 0
        Dim idiff As Integer = 0
        Dim idiff2 As Integer = 0
        Dim idaysinweek As Integer = 7


        ' iSecondHRSendDayofWeek is the day of the week we want to send the HR.  Monday is 1......Sunday is 7.  A 0 means send it out any day. iSecondHRSendDayofWeek can not be less than 0 or greater than 7 --- see cInit.vb
        ' 
        ' iSecondHRSendDays is the number of days to add to the LDC start date it is currently set to 5

        Try


            'Set the base send date as the oLDC.StartDate + iSecondHRSendDays
            dSendDate = oLDC.StartDate.AddDays(iSecondHRSendDays)

            'HE - Fix start 
            'if the vendor does off cycle drops wait until after the first read cycle to send the HR
            If oVendor.ObjectId = 83 Or oVendor.ObjectId = 411 Or oVendor.ObjectId = 412 Or oVendor.ObjectId = 413 Or oVendor.ObjectId = 471 Then
                dFirstCycleDate = getFutureReadcycle(oVendor.ObjectId, oLDC.ReadCycle, oLDC.StartDate)
                If dFirstCycleDate = Nothing Then 'no read cycle was found just add 35 days to the start date
                    dSendDate = oLDC.StartDate.AddDays(+35)
                Else
                    dSendDate = dFirstCycleDate
                End If
            End If

            If dSendDate <= Today Then
                'The oLDC.StartDate plusiSecondHRSendDays days is less or than or equal to today  so use today as the base and get the next iSecondHRSendDayofWeek. Otherwise use the  dSendDate calculated above as the base
                dSendDate = Today
            End If

            'HE Fix end


            'A iSecondHRSendDayofWeek of 0 means the HR can be sent out any day of the week so don't bother with the math  just take what as gotten above.
            If iSecondHRSendDayofWeek > 0 Then

                'At this point it does not matter whether dSendDate is today or oLDC.Startate + 5 or the first read cycle. The math is the same
                'If iDayofWeek  is Weds (3) and iSecondHRSendDayofWeek is Thursday (4) then we will have an idiff of +1 (see Case > 0).
                'If iDayofWeek is  Friday (5) and iSecondHRSendDayofWeek is Thursdy (4) then we have an idiff of  -1 (see Case < 0).
                'If iDayofWeek is Thursday (5) and iSecondHRSendDayofWeek is Thursdy (5) then we have an idiff of  0 (see Case = 0)

                iDayofWeek = dSendDate.DayOfWeek
                idiff = iSecondHRSendDayofWeek - iDayofWeek
                Select Case idiff
                    Case Is = 0
                        'Case 0 is when today is the send day of week but since we only start running the RGA at 9AM,  we have already sent out the HR's just after midnight. Therefore, the next send day becomes 1 week (7days) later
                        dSendDate = dSendDate.AddDays(+7)
                    Case Is > 0
                        'Case is greater than 0 then add that number of days to today to get to the next send date. 
                        'So if the send day is Thursday (4) and today is Weds (3) we will add 1 (idiff) to today.
                        dSendDate = dSendDate.AddDays(idiff)
                    Case Is < 0
                        'Case is less than 0.
                        'Add 7 (numnber of days in the week) to the negative idiff to get the value to add.
                        'If send day is 4 (Thurs) and today is Fri (5) the idiff will be  -1. 
                        '7 added to -1 = 6 which is the number of days you add to Friday to get the next Thursday
                        idiff2 = idaysinweek + idiff
                        dSendDate = dSendDate.AddDays(idiff2)
                End Select
            End If

            oLDC.CreateEBTHistoricalRequest("HUDT", oAccount.VendorDBNo, dSendDate, dSendDate)

        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): A general exception occurred when processing  HR EBT Transactions for LDC account # " & oLDC.LDCAcctNo & " | Error message: " & ex.Message, oLDC)
            Return False

        End Try


        Return True

    End Function   'CreateHREBT    HE-24 END

    Public Shared Function CloseEBT(ByVal oCust As RMAPI.Customer, oLDC As RMAPI.LdcAccount) As Integer



        Dim oEBTs As RMAPI.EdiTransactionList
        Dim oEBT As RMAPI.EdiTransaction
        Dim oEBTCrit As RMAPI.EbtTransactionCriteria


        Try
            'Determine whether an open rate change request exists for this rating group. If yes close it
            oEBTCrit = oLDC.GetEBTTransactionCriteria()
            oEBTCrit.LdcAcctId = oLDC.ObjectId
            oEBTCrit.TransType = "CR"
            oEBTCrit.Status = "PEND"
            oEBTCrit.RelateClassName = oLDC.ClassName()
            oEBTs = oLDC.GetEDITransactions(oEBTCrit)
            For Each oEBT In oEBTs
                If oEBT.TransactionSubType = "REFRB" Or oEBT.TransactionSubType = "MIUP" Then
                    Try
                        oEBT.CloseTransaction()
                        Return 0
                    Catch ex As Exception
                        writeToBatchLog(2, "Rating Group Tool (cFunctions): Failed to CLOSE  Change Request for LDC account # " & oLDC.LDCAcctNo & "For Rating Group = ", oLDC)
                        writeToBatchLog(2, ex.Message, oLDC)
                        Return -1
                    End Try
                End If
            Next
            If oEBTs.Count = 0 Then
                Return 2
            Else
                Return 0
            End If
        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): A general exception occurred when processing EBT Transactions for LDC account # " & oLDC.LDCAcctNo & " | Error message: " & ex.Message, oLDC)
            Return -1
        End Try
    End Function

    Public Shared Function CloseEBT_BY_DATE(ByVal oCust As RMAPI.Customer, ByVal oLDC As RMAPI.LdcAccount, ByVal dDate As Date) As Integer



        Dim oEBTs As RMAPI.EdiTransactionList
        Dim oEBT As RMAPI.EdiTransaction
        Dim oEBTCrit As RMAPI.EbtTransactionCriteria
        Dim bClose As Boolean = False
        Dim dSaveReqDate As Date = Nothing
        Dim sSaveSubType As String = Nothing
        Dim iClosedCount As Integer = 0


        Try

            oEBTCrit = oLDC.GetEBTTransactionCriteria()
            oEBTCrit.LdcAcctId = oLDC.ObjectId
            oEBTCrit.TransType = "CR"
            oEBTCrit.Status = "PEND"
            oEBTCrit.RelateClassName = oLDC.ClassName()
            oEBTs = oLDC.GetEDITransactions(oEBTCrit)
            For Each oEBT In oEBTs
                If oLDC.VendorId = 86 Then
                    If oEBT.TransactionSubType = "REFRB" And oEBT.RequestDate >= dDate Then
                        bClose = True
                        dSaveReqDate = oEBT.RequestDate
                        sSaveSubType = oEBT.TransactionSubType
                    ElseIf oEBT.TransactionSubType = "MIUP" Then
                        bClose = True
                    Else
                        bClose = False
                        Return 2
                    End If
                ElseIf oLDC.VendorId = 78 Then
                    If oEBT.TransactionSubType = "REFRB" And oEBT.RequestDate >= dDate Then
                        bClose = True
                        dSaveReqDate = oEBT.RequestDate
                        sSaveSubType = oEBT.TransactionSubType
                    ElseIf oEBT.TransactionSubType = "MIUP" Then
                        bClose = True
                    Else
                        bClose = False
                    End If
                Else
                    If (oEBT.TransactionSubType = "REFRB" Or oEBT.TransactionSubType = "MIUP") And oEBT.RequestDate >= dDate Then
                        bClose = True
                    Else
                        bClose = False
                    End If
                End If

                If bClose = True Then
                    Try
                        oEBT.CloseTransaction()
                        iClosedCount = iClosedCount + 1
                    Catch ex As Exception
                        writeToBatchLog(2, "Rating Group Tool (cFunctions): Failed to CLOSE  Change Request for LDC account # " & oLDC.LDCAcctNo & "For Rating Group = ", oLDC)
                        writeToBatchLog(2, ex.Message, oLDC)
                        Return -1
                    End Try
                End If
            Next
            If oEBTs.Count = 0 Then
                Return 2
            Else
                If iClosedCount > 0 Then
                    Return 0
                Else
                    Return 2
                End If

            End If
        Catch ex As Exception
            writeToBatchLog(2, "Rating Group Tool (cFunctions): A general exception occurred when processing EBT Transactions for LDC account # " & oLDC.LDCAcctNo & " | Error message: " & ex.Message, oLDC)
            Return -1
        End Try
    End Function

End Class
