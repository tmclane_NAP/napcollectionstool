﻿Imports NAPCollectionsTool.cInit
Imports NAPCollectionsTool.cFunctions
Imports NAPCollectionsTool.cSystem
Imports RMAPI = Nexant.BillingAPI
Imports CoreAPI = Nexant.CoreAPI
#Disable Warning IDE1006 ' Naming Styles
Public Class cMain
    'Programmer - TMcLane
    'Date       - 01/2018
    'Description  - created class
    Public Shared Sub Main()

        'Get parameters
        Dim aCommandLine As System.Collections.ObjectModel.ReadOnlyCollection(Of String)
            aCommandLine = My.Application.CommandLineArgs




            If Not init() Then
                writeToEventLog("Could not initialize NAPCollectionsTool.  Please check the configuration.", EventLogEntryType.Error)
                Exit Sub
            End If



            'Initiate Batch/Logging for RM
            oBatch = RMAPI.Batch.GetNewBatch
            oBatchRun = RMAPI.BatchRun.GetNewBatchRun
            oBatchRun.StartDateTime = Now

        writeToBatchLog(0, "Collections Tool ---- Beginning processing at " & Now & " In RM  Database = " & sDatabaseName)




        If aCommandLine.Contains("R") Then
                cReconnect.Reconnect()
            End If



        writeToBatchLog(0, "Collections Tool ---- Ending processing at " & Now & " In  RM Database = " & sDatabaseName)

        oBatchRun.EndDateTime = Now



            Try
                oBatchRun.Save()
            Catch ex As Exception
                writeToEventLog(ex.Message, EventLogEntryType.Error)
            End Try

            'Terminate Com User connection

            Try
                oComUser.Dispose()
            Catch ex As Exception
                writeToBatchLog(2, ex.Message)
            End Try


            'Terminate ODBC connections
            Try
                dbconn.Dispose()
                dbconnNR2.Dispose()
                dbconnNR3.Dispose()
                dbconnGR.Dispose()
                dbconnCE.Dispose()
            Catch ex As Exception
                writeToEventLog(ex.Message, EventLogEntryType.Error)

            End Try

        End Sub

End Class

