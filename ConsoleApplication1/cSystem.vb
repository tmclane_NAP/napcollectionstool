﻿Imports System.Data
Imports System.Data.Odbc
Imports RMAPI = Nexant.BillingAPI
Imports CoreAPI = Nexant.CoreAPI
Imports NAPCollectionsTool.cInit
'Programmer - TMcLane
'Date       - 02/2018
'Description  - created class. Copied from NAPRatingGroupTool
#Disable Warning IDE1006 ' Naming Styles
Public Class cSystem
#Enable Warning IDE1006 ' Naming Styles
    Public Shared ABPini As New cIniFile

    Public Shared oComUser As CoreAPI.User
    Public Shared sComUserId As String
    Public Shared sComPassword As String

    Public Shared sDatabaseName As String


    Public Shared dbconn As OdbcConnection
    Public Shared dbconnNR2 As OdbcConnection
    Public Shared dbconnNR3 As OdbcConnection
    Public Shared dbconnGR As OdbcConnection
    Public Shared dbconnCE As OdbcConnection


    Public Shared oBatch As RMAPI.Batch
    Public Shared oBatchRun As RMAPI.BatchRun
    Public Shared oMessage As RMAPI.BatchLogMsg


    'Opens the ABP.ini configuration file for reading
#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function setABPIniFile() As Boolean
#Enable Warning IDE1006 ' Naming Styles

        Try
            ABPini.Load("ABP.ini")
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            writeToEventLog(ex.Message, EventLogEntryType.Error)
            Return False
        End Try

        Return True

    End Function

    'Opens a SQL ODBC connection to the RevenueManager database
#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function setSqlConnection() As Boolean
#Enable Warning IDE1006 ' Naming Styles

        sODBC = ""

        Try
            sODBC = AppIni.GetSection("Database").GetKey("ODBC").Value
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
            Return False
        End Try

        If sODBC = "" Then
            writeToEventLog("ODBC entry missing in " & APP_INI_FILE_NAME & ".  Please edit the ini file and set the 'ODBC' key to the correct User DSN.", EventLogEntryType.Warning)
            Return False
        End If

        Dim connectionString = "DSN=" & sODBC & ";"

        Try
            dbconn = New OdbcConnection(connectionString)
            dbconn.Open()
            dbconnNR2 = New OdbcConnection(connectionString)
            dbconnNR2.Open()
            dbconnNR3 = New OdbcConnection(connectionString)
            dbconnNR3.Open()
            dbconnGR = New OdbcConnection(connectionString)
            dbconnGR.Open()
            dbconnCE = New OdbcConnection(connectionString)
            dbconnCE.Open()
        Catch ex As Exception
            writeToEventLog("Error opening ODBC Connection utilizing connect string :'" + connectionString + "'.  System errror: '" + ex.Message + "'", EventLogEntryType.Error)
            Return False
        End Try

        Return True

    End Function

    'Validates the Com User
#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function validateComUser() As Boolean
#Enable Warning IDE1006 ' Naming Styles

        Try
            Dim oAdminUser As CoreAPI.User
            Dim oUser As CoreAPI.User = Nothing
            Dim oSCs As CoreAPI.SecurityCodeList = Nothing
            Dim oSC As CoreAPI.SecurityCode = Nothing
            Dim bComUserFound As Boolean = False

            'Login temporarily with Admin User to validate the required Com User
            oAdminUser = CoreAPI.CoreManager.LoginEncrypted(ABPini.GetSection("REGISTRY").GetKey("ABPComLoginId").Value, _
                                                            ABPini.GetSection("REGISTRY").GetKey("ABPComPassword").Value)

            'Cycle through Users to find the required Com User
            For Each oUser In CoreAPI.User.GetAllUsers(True, "System User")
                If oUser.LoginId = COMUSER_ID Then
                    bComUserFound = True
                    sDatabaseName = oUser.DatabaseName
                    Exit For
                End If
            Next

            'Check to see if the Com User exists
            If Not bComUserFound Then
                'No Com User found
                writeToEventLog("The required Com User with Login Id '" & COMUSER_ID & "' does not exist.  Please consult the user guide on how to setup the required user.", EventLogEntryType.Error)
                Return False
            End If

            'Com User found.  Continue to check its configuration
            If Not oUser.IsActive Then
                'User is not active
                writeToEventLog("User with Login Id '" & COMUSER_ID & "' is not active.  Please activate user.  Please consult the user guide on how to setup the required user.", EventLogEntryType.Error)
                Return False
            End If

            'Check whether Com User is assigned to the correct partition group
            If oUser.PartitionGroup <> COMUSER_PARTITION_GROUP Then
                'Incorrect partition group
                writeToEventLog("User with Login Id '" & COMUSER_ID & "' is not assigned to the required '" & COMUSER_PARTITION_GROUP & "' partition group.  Please consult the user guide on how to assign the correct partition group.", EventLogEntryType.Error)
                Return False
            End If

            'Check whether necessary security codes have been assigned to Com User

            If Not oUser.IsPermissible(arCOMUSER_SECURITY_CODES(0)) And Not oUser.IsPermissible(arCOMUSER_SECURITY_CODES(1)) Then
                writeToEventLog("Required security code(s) missing for user with Login Id '" & oUser.LoginId & "'.  Please consult the user guide on how to setup the required user.", EventLogEntryType.Error)
                Return False
            End If


            'Set Login details for Com User
            sComUserId = oUser.LoginId
            sComPassword = oUser.Password

            'Close the Admin User session
            oAdminUser.Dispose()

        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
            Return False
        End Try

        Return True

    End Function

    'Logs into RevenueManager with Com User
#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function setRevenueManagerConnection() As Boolean
#Enable Warning IDE1006 ' Naming Styles

        'Initiate login for Com User
        Try
            oComUser = CoreAPI.CoreManager.Login(sComUserId, sComPassword)
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
            Return False
        End Try

        Return True

    End Function

    'Convert DateTime type to String type 'yyyymmdd'
#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function convToNexantDate(ByVal dDate As DateTime) As String
#Enable Warning IDE1006 ' Naming Styles

        Dim sYear, sMonth, sDay As String

        sYear = dDate.Year.ToString
        If dDate.Month < 10 Then
            sMonth = "0" & dDate.Month.ToString
        Else
            sMonth = dDate.Month.ToString
        End If
        If dDate.Day < 10 Then
            sDay = "0" & dDate.Day.ToString
        Else
            sDay = dDate.Day.ToString
        End If

        Return sYear & sMonth & sDay

    End Function

    'Convert String type 'yyyymmdd' to DateTime type
#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function convToDateTime(ByVal sDate As String) As DateTime
#Enable Warning IDE1006 ' Naming Styles

        Dim dDate As DateTime

        Dim iYear As Integer = System.Convert.ToInt32(sDate.Substring(0, 4))
        Dim iMonth As Integer = System.Convert.ToInt32(sDate.Substring(4, 2))
        Dim iDay As Integer = System.Convert.ToInt32(sDate.Substring(6, 2))

        dDate = New DateTime(iYear, iMonth, iDay)

        convToDateTime = dDate

    End Function

    'Writes messages to the Windows Event Log
#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function writeToEventLog(ByVal entry As String, _
                Optional ByVal eventType As EventLogEntryType = EventLogEntryType.Information) As Boolean
#Enable Warning IDE1006 ' Naming Styles

        Dim objEventLog As New EventLog

        Try

            'Register the Application as an Event Source
            'May not have permissions in registry so this is a pre-check.
            Try
                EventLog.SourceExists(APP_NAME)
            Catch ex As Exception
                Console.WriteLine("Error reading Registry entry '" + "Nexant" + "\" + APP_NAME + "'")
                Console.WriteLine("System Error :'" + ex.Message + "'")
                Console.WriteLine("Common fix: Give 'read' access for this user or everyone to registry entry 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\eventlog\Security'")
                Return False
            End Try

            If Not EventLog.SourceExists(APP_NAME) Then
                Try
                    EventLog.CreateEventSource(APP_NAME, "Nexant")
                Catch ex As Exception
                    Console.WriteLine("Error creating Event Source for'" + "Nexant" + "\" + APP_NAME + "'")
                    Console.WriteLine("System Error :'" + ex.Message + "'")
                    Console.WriteLine("Create the source manually or grant create permission to this user or everyone to the key HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\eventlog.")
                    Console.WriteLine("This permission only needs to be there for the first run of this app to create the source.  After you see a successful event log entry in 'Application and Service Logs\Nexant' you may remove this permission.")
                    Return False
                End Try
            End If

            'log the entry
            objEventLog.Source = APP_NAME
            objEventLog.WriteEntry(entry, eventType)
            Console.WriteLine("Error written to Event Log at 'Application and Service Logs\Nexant'")

            Return True

        Catch Ex As Exception
            Console.WriteLine("writeToEventLog() failed for message: '" + entry + "' because of reason: '" + Ex.Message + "'")
            Return False

        End Try

    End Function

    'Write messages to the RevenueManager Batch Log
#Disable Warning IDE1006 ' Naming Styles
    Public Shared Sub writeToBatchLog(ByVal vMessageType As Integer, ByVal vMessageText As String, Optional ByVal oLDCAcct As RMAPI.LdcAccount = Nothing)
#Enable Warning IDE1006 ' Naming Styles
        '0 = info
        '1 = warning
        '2 = error
        Dim oMessage As RMAPI.BatchLogMsg

        oMessage = RMAPI.BatchLogMsg.GetNewBatchLogMsg
        oMessage.TransType = "BATCH"
        If oLDCAcct Is Nothing Then
            'Do nothing
        Else
            oMessage.ExternalAccountNumber = oLDCAcct.LDCAcctNo
            oMessage.RelateClassName = oLDCAcct.ClassName
            oMessage.RelateId = oLDCAcct.ObjectId
        End If

        Select Case vMessageType
            Case 0
                oMessage.LoggingType = vMessageType
                oMessage.StatusCode = ""
                oMessage.MsgText = vMessageText
            Case 1
                oMessage.LoggingType = vMessageType
                oMessage.StatusCode = "OPEN"
                oMessage.MsgText = vMessageText
            Case 2
                oMessage.LoggingType = vMessageType
                oMessage.StatusCode = "OPEN"
                oMessage.MsgText = vMessageText
        End Select
        Try
            oMessage.Save()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

    End Sub

    'Gets the path of the RevenueManager application directory
#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function getRMPath() As String
#Enable Warning IDE1006 ' Naming Styles
        Const HKEY_LOCAL_MACHINE = &H80000002
        Dim sComputer As String = "."
        Dim sKeyPath As String = "SOFTWARE\Nexant\ABP"
        Dim sValueName As String = "LastInstallDir"
        Dim sValue As String = ""
        Dim oReg

        Try
            oReg = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & sComputer & "\root\default:StdRegProv")
            oReg.GetStringValue(HKEY_LOCAL_MACHINE, sKeyPath, sValueName, sValue)
        Catch ex As Exception
            writeToEventLog("Unable to find key 'HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Nexant\ABP\LastInstallDir'. System error: '" + ex.Message + "'", EventLogEntryType.Error)
        End Try

        Return sValue

    End Function

    'Validate system parameter
#Disable Warning IDE1006 ' Naming Styles
    Public Shared Function validateSystemParameter(ByVal sCode As String, ByVal vExpected As String) As Boolean
#Enable Warning IDE1006 ' Naming Styles

        Dim sQry As String
        Dim comm As OdbcCommand
        Dim rs As OdbcDataReader

        sQry = "select param_value_tx from system_parameter where param_cd = '" & sCode & "' "

        comm = New OdbcCommand(sQry, dbconn)
        rs = comm.ExecuteReader()
        rs.Read()

        If rs("param_value_tx") <> vExpected Then
            writeToEventLog("System Parameter code '" & sCode & "' is not set to the expected value ('" & vExpected & "').", EventLogEntryType.Error)
            Return False
        End If

Terminate:
        Try
            rs.Close()
            comm.Dispose()
        Catch ex As Exception
            writeToEventLog(ex.Message, EventLogEntryType.Error)
        End Try

        Return True

    End Function
End Class
